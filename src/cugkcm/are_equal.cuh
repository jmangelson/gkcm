#ifndef __CUGKCM_ARE_EQUAL_CUH
#define __CUGKCM_ARE_EQUAL_CUH

namespace cuGkCM
{

template<class CuCombT>
class ConsistentIfEqualFunctor
{
 public:

  __device__
  bool operator ()(const CuCombT& comb)
  {
    int k = CuCombT::k;
    typename CuCombT::MeasType meas = comb.get_meas(0);
    for(int i=1;i<k;i++)
    {
      typename CuCombT::MeasType next_meas = comb.get_meas(i);
      if(meas.get_value() != next_meas.get_value())
        return false;
      meas = next_meas;
    }
    return true;
  }
};

}

#endif // __CUGKCM_ARE_EQUAL_CUH
