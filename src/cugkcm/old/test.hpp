#ifndef __CUGKCM_TEST_HPP
#define __CUGKCM_TEST_HPP

#include <memory>

#include "gkcm/gkcm.hpp"

namespace GkCM
{

///////////////////////////////////////////////////////////
// The Host Measurement Object
//
// This class represents a single measurement
class IntMeasurement
{
 private:
  int value;
  
 public:
  IntMeasurement(int val): value(val) {}

  IntMeasurement(const IntMeasurement& other)
      : value(other.get_value())
  {}

  int get_value() const {return value;}
};


///////////////////////////////////////
// Some Important Forward Declarations
class AreIntsEqualEvaluatorCu;

std::unique_ptr<AreIntsEqualEvaluatorCu>
AreIntsEqualEvaluatorCu_Factory();

std::vector< std::vector<int> >
AreIntsEqualEvaluatorCu::evaluate_consistency_with_all_measurements(
    std::vector<int> measurements_to_check,
    const std::vector<IntMeasurement>& measurement_map,
    bool sort=false);


//////////////////////////////////////////////////
// The GCC Accessible ConsistencyEvaluator class
//

class AreIntsEqualEvaluator : ConsistencyEvaluator<IntMeasurement>
{
 private:
  std::unique_ptr<AreIntsEqualEvaluatorCu> cu_evaluator;
  
 public:
  AreIntsEqualEvaluator():
      ConsistencyEvaluator<IntMeasurement> (2),
      cu_evaluator(AreIntsEqualEvaluatorCu_Factory())
  {}
  ~AreIntsEqualEvaluator();
  
  // Define function for pure virtual evaluate_consistency
  // This will never be called
  bool evaluate_consistency(std::vector<int> measurements,
                            const std::vector<IntMeasurement>& measurement_map)
  {
    return false;
  }

  // Overwrite evaluate_consistency_with_all_measurements function
  // to use the gpu
  std::vector< std::vector<int> > evaluate_consistency_with_all_measurements(
      std::vector<int> measurements_to_check,
      const std::vector<IntMeasurement>& measurement_map,
      bool sort=false)
  {
    return this->cu_evaluator.evaluate_consistency_with_all_measurements(measurements_to_check,
                                                                         measurement_map,
                                                                         sort);
  }
  
};


}; // namespace GkCM

#endif // __CUGKCM_TEST_HPP
