#ifndef __CUBLAS_CPP_CUH
#define __CUBLAS_CPP_CUH

#include <stdlib.h>
#include <memory>

#include <thrust/device_malloc.h>
#include <thrust/device_ptr.h>
#include <thrust/device_free.h>
#include <thrust/copy.h>

#include <cublas_v2.h>

#include <Eigen/Core>


namespace cuBlasCPP
{

template<typename _Scalar, int _Rows, int _Cols>
class Matrix
{
 public:
  typedef _Scalar ScalarType;
  static const int rows = _Rows;
  static const int cols = _Cols;

 private:
  thrust::device_ptr<ScalarType> data_dp;
  std::vector<_Scalar> data_hv;  

 public:
  __host__ __device__
  Matrix() :
      data_dp(),
      data_hv()
      
#ifdef __CUDA_ARCH__
      data_p(thrust::device_malloc<_Scalar>(_Rows * _Cols))
#else
      data_p(new _Scalar[rows*cols])
#endif
  {}

  __host__ __device__
  ~Matrix()
  {
#ifdef __CUDA_ARCH__
    thrust::device_free(data_p);
#endif    
  }

  __host__ __device__
  Matrix(const Matrix& other) :
#ifdef __CUDA_ARCH__
      data_p(thrust::device_malloc<_Scalar>(_Rows *_Cols))
#else
      data_p(new _Scalar[rows*cols])
#endif      
  {
    thrust::copy(other.data_p, other.data_p+rows*cols, this->data_p);
  }

  __host__ 
  Matrix(const Eigen::Matrix<_Scalar, _Rows, _Cols>& matrix) :
#ifdef __CUDA_ARCH__
      data_p(thrust::device_malloc<_Scalar>(_Rows * _Cols))
#else
      data_p(new _Scalar[rows*cols])
#endif            
  {
    thrust::copy(matrix.data(), matrix.data()+rows*cols, this->data_p.get());
  }

  __host__ 
  Eigen::Matrix<_Scalar, _Rows, _Cols> convert_to_eigen_matrix()
  {
    Eigen::Matrix<_Scalar, _Rows, _Cols> mat =
        Eigen::Map<Eigen::Matrix<_Scalar, _Rows, _Cols> >(this->data_p.get());
    return mat;
  }  
  
};

} // namespace cuBlasCPP

#endif // __CUBLAS_CPP_CUH
