#ifndef __CUGKCM_INT_CE_INSTANTIATIONS_CU
#define __CUGKCM_INT_CE_INSTANTIATIONS_CU

#include "cugkcm.cuh"

#include "are_equal.cuh"


///////////////////////////////////////////////////////////////
// Define Macro for instantiating measurements, combinations,
//  and evaluation_consistency_via_thrust function for a given
//  type and K value
///////////////////////////////////////////////////////////////

#define INSTANTIATE(T, K)  \
template class cuGkCM::CuMeasurement<T>; \
template class GkCM::Combination<GkCM::Measurement<T>, K>; \
template class cuGkCM::CuCombination< cuGkCM::CuMeasurement<T>, K>; \
template \
std::vector<bool> \
cuGkCM::evaluate_consistency_via_thrust< \
   GkCM::Combination<GkCM::Measurement<T>, K>, \
   cuGkCM::CuCombination< cuGkCM::CuMeasurement<T>, K>, \
   cuGkCM::ConsistentIfEqualFunctor< cuGkCM::CuCombination< cuGkCM::CuMeasurement<T>, K> > >( \
       std::vector< GkCM::Combination<GkCM::Measurement<T>, K> >);

///////////////////////////////////
// Instantiate the standard types
///////////////////////////////////
INSTANTIATE(int, 2)
INSTANTIATE(double, 2)
INSTANTIATE(char, 2)
INSTANTIATE(bool, 2)
INSTANTIATE(float, 2)    


#undef INSTANTIATE

#endif // __CUGKCM_INT_CE_INSTANTIATIONS_CU
