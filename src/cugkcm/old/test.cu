#include "test.cuh"

#undef NDEBUG
#include <assert.h>

__host__ __device__
bool
GkCM::AreIntsEqualFunctor::evaluate_consistency(GkCM::IntPair& comb)
{
  return comb.get_a().get_value() == comb.get_b().get_value();
}

GkCM::IntPair
GkCM::AreIntsEqualEvaluatorCu::get_data_for_combination(
    std::vector<int> combination,
    const std::vector<GkCM::IntMeasurement>& measurement_map)
{
  assert(combination.size() == 2);
  IntPair tmp(measurement_map(combination[0]),
              measurement_map(combination[1]);
  return tmp;
}
