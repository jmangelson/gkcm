#ifndef __CUGKCM_INT_COMBINATIONS_CUH
#define __CUGKCM_INT_COMBINATIONS_CUH

#include "gkcm/gkcm.hpp"

namespace cuGkCM
{

////////////////////////////////////////
// 
////////////////////////////////////////

////////////////////////////////////////
// Int Pair
////////////////////////////////////////
class CuCombination_int_2 : 
{
 private:
  int val0;
  int val1;
  
 public:
  __host__ __device__
  CuCombination_int_2() {}
  __host__ __device__
  ~CuCombination_int_2() {}  

  __host__ __device__
  CuCombination_int_2(const CuCombination_int_2& other)
      : val0(other.get_meas(0)),
        val1(other.get_meas(1))
  {}
  
  __host__ __device__
  CuCombination_int_2(const GkCM::Combination< GkCM::Measurement<int>, 2>& host_comb)
      : val0(host_comb.get_meas(0).get_value()),
        val1(host_comb.get_meas(1).get_value())
  {}
  
  __host__ __device__
  int get_meas(int index) const
  {
    assert(index >= 0 && index <=1);
    if(index == 0) return val0;
    else return val1;
  }
};

};

#endif // __CUGKCM_INT_COMBINATIONS_CUH
