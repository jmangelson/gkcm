#ifndef __CUGKCM_TEST_CUH
#define __CUGKCM_TEST_CUH

#include <memory>

#include "cugkcm.cuh"

#include "test.hpp"

// For a given cuda based consistency evaluation
// implementation, three files need to be implemented:
// .cuh - This file defines the necessary classes
//        including:
//          - The Measurement Class
//          - The CombinationData child class
//          - The ConsistencyEvalFunctor
//          - The CuConsistencyEvaluator child class
//
// .cu  - This file instantiates the necessary
//        templates and implements non-template
//        functions
//
// .hpp - This class forward declares the non-cuda
//        functions that can be accessed from external
//        .cpp files linking against the cugkcm
//        library, but compiled with gcc
//
// The .cuh and .cu files are compiled with nvcc
// and the .hpp are included elsewhere and can be
// compiled with gcc
//
//See the following for more information:
//  https://stackoverflow.com/questions/28410321/using-function-templated-code-across-the-g-nvcc-boundary-including-kernels

namespace GkCM
{

///////////////////////////////////////////////////////////
// The CUDA Measurement Object
//
// This class represents a single measurement (in this case
// of an integer value)
//
class IntMeasurementCu
{
 private:
  int value;
  
 public:
  __host__ __device__
  IntMeasurementCu(int val): value(val) {}

  __host__ __device__
  IntMeasurementCu(const IntMeasurementCu& other)
      : value(other.get_value());
  {}
  
  __host__ __device__
  IntMeasurementCu(const IntMeasurement& host_measurement)
      : value(host_measurement.get_value());
  {}

  int get_value() const {return value;}
};

///////////////////////////////////////////////////////////
// Measurement Combination (extends CombinationData)
//
// This class contains all data needed to evaluate
// consistency of a given k-tuple (in this case pair) of
// measurements.
class IntPair : public CombinationData<IntMeasurementCu>
{
 private:
  IntMeasurementCu a;
  IntMeasurementCu b;  
  
 public:
  __host__ __device__
  IntPair(const IntMeasurementCu& meas_a,
          const IntMeasurementCu& meas_b)
      : a(meas_a),
        b(meas_b)
  {}
  __host__ __device__
  ~IntPair() {}   

  __host__ __device__ 
  IntPair(const IntPair& other)
      : a(other.get_a()),
        b(other.get_b()),
  {}

  IntMeasurementCu get_a() {return a;}
  IntMeasurementCu get_b() {return b;}  
};

///////////////////////////////////////////////////////////
// Functor For Evaluating Consistency on the Device
//
// The operator () must be defined to take in the data
// for a k-tuple of measurements (TestCombination in this
// case), evaluate its consistency and return a bool that
// is true if the k-tuple of measurements is consistent
class AreIntsEqualFunctor
{
 public:
  __host__ __device__
  bool evaluate_consistency(IntPair& comb);
  
  __host__ __device__
  bool operator ()(IntPair& comb)
  {
    return this->evaluate_consistency(comb);
  }
};

///////////////////////////////////////////////////////////
// CuConsistencyEvaluator child class
//
// In addition to the constructor, this class needs to
// implement the get_data_for_combination function that
// takes a combination of measurements (specified by
// indices and a measurement_map) and returns a
// instance of the TestCombination class that contains
// all necessary information for evaluating consistency
class AreIntsEqualEvaluatorCu :
      public ConsistencyEvaluatorCu<IntMeasurementCu,
                                    IntPair,
                                    AreIntsEqualFunctor>
{
 public:
  AreIntsEqualEvaluatorCu() :
      CuConsistencyEvaluator<IntMeasurementCu,
                             IntPair,
                             AreIntsEqualFunctor> (2)
  {}
  
  IntPair get_data_for_combination(
      std::vector<int> combination,
      const std::vector<IntMeasurement>& measurement_map);      
};

std::unique_ptr<AreIntsEqualEvaluatorCu>
AreIntsEqualEvaluatorCu_Factory()
{
  std::unique_ptr<AreIntsEqualEvaluatorCu>
      evaluator(new AreIntsEqualEvaluatorCu());
  return evaluator;
}

std::vector< std::vector<int> >
eval_consistency_with_all_via_AreIntsEqualEvaluatorCu(
    std::vector<int> measurements_to_check,
    const std::vector<IntMeasurement>& measurement_map,
    bool sort=false)
{
  
}


} // namespace GkCM

#endif // __CUGKCM_TEST_CUH
