#ifndef __CUGKCM_HPP
#define __CUGKCM_HPP

#include <vector>
#include <type_traits>

#include "gkcm/gkcm.hpp"

namespace cuGkCM
{

////////////////////////////////////////////////////////////
// Forward Declarations of Cuda template objects/functions
////////////////////////////////////////////////////////////

// Forward Declare Measurements
template<typename DataT>
class CuMeasurement;

// Forward Declare Combinations
template<typename CuMeasT, int K>
class CuCombination;

//Forward Declare evaluation function;
template <class hostCombinationT, class cuCombinationT, class cuEvaluationFunctorT>
std::vector< bool >
evaluate_consistency_via_thrust( std::vector< hostCombinationT > combinations);

// Forward Declare Functors
template<class CuCombT> class ConsistentIfEqualFunctor;

//////////////////////////
// Dummy Functor
//////////////////////////
template <class CombT>
class DummyFunctor {
 public:
  bool operator ()(const CombT& comb)
  {
    std::cerr << "This should never get called.\n";
    exit(-1);
  }
};

///////////////////////////////////////////////////////////////
// Macro for defining ConsistencyEvaluator for a given functor
//////////////////////////////////////////////////////////////
#define CE_TEMPLATE_FOR_FUNCTOR(CE_NAME, FUNCTOR_T)  \
template<typename DataT, int K> \
class CE_NAME : \
      public GkCM::ConsistencyEvaluator<GkCM::Measurement<DataT>,       \
                                        GkCM::Combination<GkCM::Measurement<DataT>, K>, \
                                        DummyFunctor< GkCM::Combination<GkCM::Measurement<DataT>, K> > > \
{ \
  static_assert((std::is_same<DataT, int>::value || std::is_same<DataT, double>::value || std::is_same<DataT, char>::value || \
                 std::is_same<DataT, bool>::value || std::is_same<DataT, float>::value), \
                " CE_NAME implemented for template parameter DataT."); \
  static_assert((K==2), " CE_NAME currently not implemented for K other than 2."); \
 public: \
  CE_NAME() {} \
  ~CE_NAME() {} \
 \
  std::vector<bool> \
      evaluate_consistency_of_combinations( \
          std::vector< GkCM::Combination<GkCM::Measurement<DataT>, K> > combinations) \
  { \
    std::vector<bool> results = \
        cuGkCM::evaluate_consistency_via_thrust< \
          GkCM::Combination< GkCM::Measurement<DataT>, K>, \
      cuGkCM::CuCombination< cuGkCM::CuMeasurement<DataT>, K>, \
      cuGkCM::FUNCTOR_T< cuGkCM::CuCombination< cuGkCM::CuMeasurement<DataT>, K> > >(combinations); \
 \
    return results; \
  } \
};

//////////////////////////////////////////////
// Generate Template for Implemented Functors
//////////////////////////////////////////////

//Generates the template for
// template<typename DataT, int K> cuGkCM::AreEqualConsistencyEvaluator
CE_TEMPLATE_FOR_FUNCTOR( AreEqualConsistencyEvaluator, ConsistentIfEqualFunctor )

#undef CE_TEMPLATE_FOR_FUNCTOR


} // namespace cuGkCM

#endif // __CUGKCM_HPP
