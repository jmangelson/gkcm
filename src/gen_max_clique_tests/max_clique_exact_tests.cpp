#include "gen_max_clique/gmc.hpp"

#include <random>

#include "gtest/gtest.h"

TEST(MaxCliqueTest, SimpleK2Graph)
{
  GMC::KUniformHypergraph<int> graph(2);

  graph.add_nodes({0, 1, 2, 3, 4});
  graph.add_edge({2, 3});
  graph.add_edge({2, 4});
  graph.add_edge({3, 4});

  GMC::MaxCliqueSolverExact<int> solver;
  auto clique = solver.max_clique(graph, true);
  GMC::print(std::cout, clique);
  std::cout << "\n";

  ASSERT_EQ(clique, std::vector<int>({2, 3, 4}));
}


TEST(MaxCliqueTest, SimpleK3Graph)
{
  GMC::KUniformHypergraph<int> graph(3);

  graph.add_nodes({1, 2, 3, 4, 5, 6, 7});
  graph.add_edge({1, 2, 5});
  graph.add_edge({1, 2, 4});
  graph.add_edge({1, 4, 5});
  graph.add_edge({2, 4, 5});
  graph.add_edge({1, 3, 5});
  graph.add_edge({5, 6, 7});
  graph.add_edge({4, 5, 7});

  GMC::MaxCliqueSolverExact<int> solver;
  auto clique = solver.max_clique(graph, true);
  GMC::print(std::cout, clique);
  std::cout << "\n";

  ASSERT_EQ(clique, std::vector<int>({1, 2, 4, 5}));
}

TEST(MaxCliqueTest, Random30NodeK4Graph)
{
  GMC::KUniformHypergraph<int> graph(4);

  std::vector<int> nodes;
  for(int i=0;i<30;i++)
  {
    nodes.push_back(i);
    graph.add_node(i);
  }

  // Create max clique
  std::vector<int> max_clique;
  for(int n=0;n<15;n+=2)
  {
    max_clique.push_back(n);
  }

  auto clique_edges = GMC::combinations_of_size(max_clique, 4);
  for( auto e : clique_edges )
  {
    graph.add_edge(e);
  }

  // Add random edges
  for(int i=0;i<800;i++)
  {
    std::shuffle(nodes.begin(), nodes.end(), std::default_random_engine(i));

    for(int j=0;j<7;j++)
    {
      std::vector<int> e = {nodes[j*4], nodes[j*4+1], nodes[j*4+2], nodes[j*4+3]};
      graph.add_edge(e);
    }
  }

  // Find clique
  GMC::MaxCliqueSolverExact<int> solver;
  auto clique = solver.max_clique(graph, true);
  GMC::print(std::cout, clique);
  std::cout << "\n";

  ASSERT_EQ(clique, max_clique);
}

TEST(MaxCliqueTest, Random50NodeK3Graph)
{
  GMC::KUniformHypergraph<int> graph(3);

  std::vector<int> nodes;
  for(int i=0;i<50;i++)
  {
    nodes.push_back(i);
    graph.add_node(i);
  }

  // Create max clique
  std::vector<int> max_clique;
  std::shuffle(nodes.begin(), nodes.end(), std::default_random_engine(100021));
  for(int i=0;i<10; i++)
  {
    max_clique.push_back(nodes[i]);
  }
  std::sort(max_clique.begin(), max_clique.end());

  auto clique_edges = GMC::combinations_of_size(max_clique, 3);
  for( auto e : clique_edges )
  {
    graph.add_edge(e);
  }

  // Add random edges
  auto all_edges = GMC::combinations_of_size(nodes, 3, true);
  std::shuffle(all_edges.begin(), all_edges.end(), std::default_random_engine(0));

  double density = 0.2;
  std::vector< std::vector<int> > added_edges;
  for(int i=0; i < density*all_edges.size();i++)
  {
    added_edges.push_back(all_edges[i]);
  }

  graph.add_edges(added_edges);

  // Find clique
  GMC::MaxCliqueSolverExact<int> solver;
  auto clique = solver.max_clique(graph, true);
  GMC::print(std::cout, clique);
  std::cout << "\n";

  ASSERT_EQ(clique, max_clique);
}

TEST(ParallelMaxCliqueTest, SimpleK3Graph)
{
  GMC::KUniformHypergraph<int> graph(3);

  graph.add_nodes({1, 2, 3, 4, 5, 6, 7});
  graph.add_edge({1, 2, 5});
  graph.add_edge({1, 2, 4});
  graph.add_edge({1, 4, 5});
  graph.add_edge({2, 4, 5});
  graph.add_edge({1, 3, 5});
  graph.add_edge({5, 6, 7});
  graph.add_edge({4, 5, 7});

  // Create solver with multiple threads
  GMC::MaxCliqueSolverExact<int> solver(8);
  auto clique = solver.max_clique(graph, true);
  GMC::print(std::cout, clique);
  std::cout << "\n";

  ASSERT_EQ(clique, std::vector<int>({1, 2, 4, 5}));
}


TEST(ParallelMaxCliqueTest, Random30NodeK4Graph)
{
  GMC::KUniformHypergraph<int> graph(4);

  std::vector<int> nodes;
  for(int i=0;i<30;i++)
  {
    nodes.push_back(i);
    graph.add_node(i);
  }

  // Create max clique
  std::vector<int> max_clique;
  for(int n=0;n<15;n+=2)
  {
    max_clique.push_back(n);
  }

  auto clique_edges = GMC::combinations_of_size(max_clique, 4);
  for( auto e : clique_edges )
  {
    graph.add_edge(e);
  }

  // Add random edges
  for(int i=0;i<800;i++)
  {
    std::shuffle(nodes.begin(), nodes.end(), std::default_random_engine(i));

    for(int j=0;j<7;j++)
    {
      std::vector<int> e = {nodes[j*4], nodes[j*4+1], nodes[j*4+2], nodes[j*4+3]};
      graph.add_edge(e);
    }
  }

  // Find clique
  // Create solver with multiple threads
  GMC::MaxCliqueSolverExact<int> solver(8);
  auto clique = solver.max_clique(graph, true);
  GMC::print(std::cout, clique);
  std::cout << "\n";

  ASSERT_EQ(clique, max_clique);
}


TEST(ParallelMaxCliqueTest, Random50NodeK3Graph)
{
  GMC::KUniformHypergraph<int> graph(3);

  std::vector<int> nodes;
  for(int i=0;i<50;i++)
  {
    nodes.push_back(i);
    graph.add_node(i);
  }

  // Create max clique
  std::vector<int> max_clique;
  std::shuffle(nodes.begin(), nodes.end(), std::default_random_engine(100021));
  for(int i=0;i<10; i++)
  {
    max_clique.push_back(nodes[i]);
  }
  std::sort(max_clique.begin(), max_clique.end());

  auto clique_edges = GMC::combinations_of_size(max_clique, 3);
  for( auto e : clique_edges )
  {
    graph.add_edge(e);
  }

  // Add random edges
  auto all_edges = GMC::combinations_of_size(nodes, 3, true);
  std::shuffle(all_edges.begin(), all_edges.end(), std::default_random_engine(0));

  double density = 0.2;
  std::vector< std::vector<int> > added_edges;
  for(int i=0; i < density*all_edges.size();i++)
  {
    added_edges.push_back(all_edges[i]);
  }

  graph.add_edges(added_edges);

  // Find clique
  // Create solver with multiple threads
  GMC::MaxCliqueSolverExact<int> solver(8);
  auto clique = solver.max_clique(graph, true);
  GMC::print(std::cout, clique);
  std::cout << "\n";

  ASSERT_EQ(clique, max_clique);
}


TEST(K2Test, PairwiseGraph)
{
  GMC::KUniformHypergraph<int> graph(2);

  graph.add_nodes({1, 2, 3, 4, 5, 6, 7, 8});
  graph.add_edges({{1, 2}, {2, 3}, {2,4}, {2,5}, {3, 4}, {3, 5}, {4, 5}, {2, 8}, {7, 6}, {6, 3}, {8, 1}});


  GMC::MaxCliqueSolverExact<int> solver;
  auto clique = solver.max_clique(graph, true);

  GMC::print(std::cout, clique);

  ASSERT_EQ(clique, std::vector<int>({2, 3, 4, 5}));
}

TEST(OffsetByOneTest, CornerCase1)
{
  GMC::KUniformHypergraph<int> graph(2);

  graph.add_nodes({1, 2, 3, 4, 5, 6});
  graph.add_edges({{1, 4}, {2, 3}, {2,5}, {3,5}});


  GMC::MaxCliqueSolverExact<int> solver;
  auto clique = solver.max_clique(graph, true);

  GMC::print(std::cout, clique);

  ASSERT_EQ(clique, std::vector<int>({2, 3, 5}));
}

TEST(ParallelOffsetByOneTest, CornerCase1)
{
  GMC::KUniformHypergraph<int> graph(2);

  graph.add_nodes({1, 2, 3, 4, 5, 6});
  graph.add_edges({{1, 4}, {2, 3}, {2,5}, {3,5}});


  GMC::MaxCliqueSolverExact<int> solver(4);
  auto clique = solver.max_clique(graph, true);

  GMC::print(std::cout, clique);

  ASSERT_EQ(clique, std::vector<int>({2, 3, 5}));
}

TEST(IncrementalCliqueTest, PairwiseGraph)
{
  GMC::KUniformHypergraph<int> graph(2);

  graph.add_nodes({1, 2, 3, 4, 5, 6, 7, 8});
  graph.add_edges({{1, 2}, {2, 3}, {2,4}, {2,5}, {3, 4}, {3, 5}, {4, 5}, {2, 8}, {7, 6}, {6, 3}, {8, 1}});


  GMC::MaxCliqueSolverExact<int> solver;
  auto clique = solver.max_clique(graph, true);

  std::vector<int> new_meas{9, 10};
  graph.add_nodes(new_meas);
  graph.add_edges({{2, 9}, {3, 9}, {4, 9}, {5, 9}, {1, 10}, {8, 10}});
  auto clique2 = solver.solveIncremental(graph, new_meas);

  GMC::print(std::cout, clique);
  GMC::print(std::cout, clique2);

  ASSERT_EQ(clique, std::vector<int>({2, 3, 4, 5}));
  ASSERT_EQ(clique2, std::vector<int>({2, 3, 4, 5, 9}));
}

TEST(IncrementalCliqueTest, PairwiseGraphMultiThread)
{
  GMC::KUniformHypergraph<int> graph(2);

  graph.add_nodes({1, 2, 3, 4, 5, 6, 7, 8});
  graph.add_edges({{1, 2}, {2, 3}, {2,4}, {2,5}, {3, 4}, {3, 5}, {4, 5}, {2, 8}, {7, 6}, {6, 3}, {8, 1}});


  GMC::MaxCliqueSolverExact<int> solver(3);
  auto clique = solver.max_clique(graph, true);

  std::vector<int> new_meas{9, 10};
  graph.add_nodes(new_meas);
  graph.add_edges({{2, 9}, {3, 9}, {4, 9}, {5, 9}, {1, 10}, {8, 10}});
  auto clique2 = solver.solveIncremental(graph, new_meas);

  GMC::print(std::cout, clique);
  GMC::print(std::cout, clique2);

  ASSERT_EQ(clique, std::vector<int>({2, 3, 4, 5}));
  ASSERT_EQ(clique2, std::vector<int>({2, 3, 4, 5, 9}));
}

TEST(IncrementalCliqueTest, ThreewayGraph)
{
    GMC::KUniformHypergraph<int> graph(3);

    graph.add_nodes({1, 2, 3, 4, 5, 6, 7, 8});
    graph.add_edges({{1, 2, 3}, {2, 3, 4}, {2, 3, 5}, {2, 4, 5}, {3, 4, 5}, {6, 7, 8}, {1, 7, 8}, {4, 6, 7}, {1, 6, 7}});

    GMC::MaxCliqueSolverExact<int> solver;
    auto clique = solver.max_clique(graph, true);

    std::vector<int> new_meas{9, 10};
    graph.add_nodes(new_meas);
    graph.add_edges({{2, 3, 9}, {2, 4, 9}, {2, 5, 9}, {3, 4, 9}, {3, 5, 9}, {4, 5, 9}, {1, 8, 10}, {7, 8, 10}, {6, 9, 10}});
    auto clique2 = solver.max_clique_incremental(graph, new_meas);

    GMC::print(std::cout, clique);
    GMC::print(std::cout, clique2);

    ASSERT_EQ(clique, std::vector<int>({2, 3, 4, 5}));
    ASSERT_EQ(clique2, std::vector<int>({2, 3, 4, 5, 9}));

}

TEST(IncrementalCliqueTest, ChangingMaxClique) {
    GMC::KUniformHypergraph<int> graph(3);

    graph.add_nodes({1, 2, 3, 4, 5, 6, 7, 8});
    graph.add_edges({{1, 2, 3}, {2, 3, 4}, {2, 3, 5}, {2, 4, 5}, {3, 4, 5}, {6, 7, 8}, {1, 7, 8}, {4, 6, 7}, {1, 6, 7}});

    GMC::MaxCliqueSolverExact<int> solver;
    auto clique = solver.max_clique(graph, true);

    std::vector<int> new_meas{9, 10};
    graph.add_nodes(new_meas);
    graph.add_edges({{2, 3, 9}, {2, 4, 9}, {2, 5, 9}, {3, 4, 9}, {3, 5, 9}, {4, 5, 9}, {1, 6, 8}, {1, 6, 9}, {1, 6, 10}, {1, 7, 9}, {1, 7, 10}, {1, 8, 9}, {1, 8, 10}, {1, 9, 10}, {6, 7, 9}, {6, 7, 10}, {6, 8, 9}, {6, 8, 10}, {6, 9, 10}, {7, 8, 9}, {7, 8, 10}, {7, 9, 10}, {8, 9, 10}});
    auto clique2 = solver.max_clique_incremental(graph, new_meas);

    GMC::print(std::cout, clique);
    GMC::print(std::cout, clique2);

    ASSERT_EQ(clique, std::vector<int>({2, 3, 4, 5}));
    ASSERT_EQ(clique2, std::vector<int>({1, 6, 7, 8, 9, 10}));

}
