#include "gen_max_clique/kuniform_hypergraph.hpp"

#include "gtest/gtest.h"

TEST(AddNodeTest, AddNode)
{
  GMC::KUniformHypergraph<int> graph(3);

  ASSERT_EQ(graph.num_nodes(), 0);
  graph.add_node(0);
  graph.add_node(1);

  ASSERT_EQ(graph.num_nodes(), 2);
  ASSERT_TRUE(graph.contains_node(0));
  ASSERT_TRUE(graph.contains_node(1));
  ASSERT_FALSE(graph.contains_node(2));  
}


TEST(AddNodeTest, AddNodes)
{
  GMC::KUniformHypergraph<int> graph(3);

  ASSERT_EQ(graph.num_nodes(), 0);
  graph.add_nodes({0,1});

  ASSERT_EQ(graph.num_nodes(), 2);
  ASSERT_TRUE(graph.contains_node(0));
  ASSERT_TRUE(graph.contains_node(1));
  ASSERT_FALSE(graph.contains_node(2));  
}


TEST(AddNodeDeathTest, AddAddedNode)
{
  GMC::KUniformHypergraph<int> graph(3);

  graph.add_node(1);
  ASSERT_DEATH(graph.add_node(1), "");
}

TEST(AddNodeDeathTest, AddAddedNodes)
{
  GMC::KUniformHypergraph<int> graph(3);

  graph.add_node(1);

  ASSERT_DEATH(graph.add_nodes({0,1}), "");
}


TEST(AddEdgeTest, AddEdge)
{
  GMC::KUniformHypergraph<char> graph(3);

  graph.add_nodes({'a', 'b', 'c'});

  // Add One Edge
  graph.add_edge({'a', 'b', 'c'});

  ASSERT_EQ(graph.edge_set('a')[0], std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.edge_set('a').size(), 1);
  ASSERT_EQ(graph.edge_set('b')[0], std::vector<char>({'a', 'c'}));
  ASSERT_EQ(graph.edge_set('b').size(), 1);
  ASSERT_EQ(graph.edge_set('c')[0], std::vector<char>({'a', 'b'}));
  ASSERT_EQ(graph.edge_set('c').size(), 1);

  ASSERT_EQ(graph.neighborhood('a'), std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.neighborhood('b'), std::vector<char>({'a', 'c'}));
  ASSERT_EQ(graph.neighborhood('c'), std::vector<char>({'a', 'b'}));

  auto edges = graph.get_edges();
  ASSERT_EQ(edges.size(), 1);
  ASSERT_EQ(edges[0], std::vector<char>({'a', 'b', 'c'}));


  // Add Another Edge
  
  graph.add_node('d');
  graph.add_edge({'b', 'c', 'd'});

  ASSERT_EQ(graph.edge_set('a')[0], std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.edge_set('a').size(), 1);
  ASSERT_TRUE(GMC::contains(graph.edge_set('b'), std::vector<char>({'a', 'c'})));
  ASSERT_TRUE(GMC::contains(graph.edge_set('b'), std::vector<char>({'c', 'd'})));
  ASSERT_EQ(graph.edge_set('b').size(), 2);
  ASSERT_TRUE(GMC::contains(graph.edge_set('c'), std::vector<char>({'a', 'b'})));
  ASSERT_TRUE(GMC::contains(graph.edge_set('c'), std::vector<char>({'b', 'd'})));  
  ASSERT_EQ(graph.edge_set('c').size(), 2);
  ASSERT_TRUE(GMC::contains(graph.edge_set('d'), std::vector<char>({'b', 'c'})));  
  ASSERT_EQ(graph.edge_set('d').size(), 1);

  ASSERT_EQ(graph.neighborhood('a'), std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.neighborhood('b'), std::vector<char>({'a', 'c', 'd'}));
  ASSERT_EQ(graph.neighborhood('c'), std::vector<char>({'a', 'b', 'd'}));
  ASSERT_EQ(graph.neighborhood('d'), std::vector<char>({'b', 'c'}));
  
  edges = graph.get_edges();
  ASSERT_EQ(edges.size(), 2);
  ASSERT_EQ(edges[0], std::vector<char>({'a', 'b', 'c'}));
  ASSERT_EQ(edges[1], std::vector<char>({'b', 'c', 'd'}));
  

  // Check duplicate adding of edges
  graph.add_edge({'d', 'c', 'b'});
  ASSERT_EQ(graph.edge_set('a')[0], std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.edge_set('a').size(), 1);
  ASSERT_TRUE(GMC::contains(graph.edge_set('b'), std::vector<char>({'a', 'c'})));
  ASSERT_TRUE(GMC::contains(graph.edge_set('b'), std::vector<char>({'c', 'd'})));
  ASSERT_EQ(graph.edge_set('b').size(), 2);
  ASSERT_TRUE(GMC::contains(graph.edge_set('c'), std::vector<char>({'a', 'b'})));
  ASSERT_TRUE(GMC::contains(graph.edge_set('c'), std::vector<char>({'b', 'd'})));  
  ASSERT_EQ(graph.edge_set('c').size(), 2);
  ASSERT_TRUE(GMC::contains(graph.edge_set('d'), std::vector<char>({'b', 'c'})));  
  ASSERT_EQ(graph.edge_set('d').size(), 1);

  edges = graph.get_edges();
  ASSERT_EQ(edges.size(), 2);
  ASSERT_EQ(edges[0], std::vector<char>({'a', 'b', 'c'}));
  ASSERT_EQ(edges[1], std::vector<char>({'b', 'c', 'd'}));

  ASSERT_EQ(graph.neighborhood('a'), std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.neighborhood('b'), std::vector<char>({'a', 'c', 'd'}));
  ASSERT_EQ(graph.neighborhood('c'), std::vector<char>({'a', 'b', 'd'}));
  ASSERT_EQ(graph.neighborhood('d'), std::vector<char>({'b', 'c'}));

}

TEST(AddEdgeTest, AddEdges)
{
  GMC::KUniformHypergraph<char> graph(3);

  graph.add_nodes({'a', 'b', 'c'});

  // Add 2 Edges
  
  graph.add_node('d');
  graph.add_edges({{'a', 'b', 'c'},{'b', 'c', 'd'}});

  ASSERT_EQ(graph.edge_set('a').size(), 1);
  ASSERT_EQ(graph.edge_set('a')[0], std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.edge_set('b').size(), 2);
  ASSERT_TRUE(GMC::contains(graph.edge_set('b'), std::vector<char>({'a', 'c'})));
  ASSERT_TRUE(GMC::contains(graph.edge_set('b'), std::vector<char>({'c', 'd'})));
  ASSERT_EQ(graph.edge_set('c').size(), 2);
  ASSERT_TRUE(GMC::contains(graph.edge_set('c'), std::vector<char>({'a', 'b'})));
  ASSERT_TRUE(GMC::contains(graph.edge_set('c'), std::vector<char>({'b', 'd'})));  
  ASSERT_EQ(graph.edge_set('d').size(), 1);
  ASSERT_TRUE(GMC::contains(graph.edge_set('d'), std::vector<char>({'b', 'c'})));  


  ASSERT_EQ(graph.neighborhood('a'), std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.neighborhood('b'), std::vector<char>({'a', 'c', 'd'}));
  ASSERT_EQ(graph.neighborhood('c'), std::vector<char>({'a', 'b', 'd'}));
  ASSERT_EQ(graph.neighborhood('d'), std::vector<char>({'b', 'c'}));
  
  auto edges = graph.get_edges();
  ASSERT_EQ(edges.size(), 2);
  ASSERT_EQ(edges[0], std::vector<char>({'a', 'b', 'c'}));
  ASSERT_EQ(edges[1], std::vector<char>({'b', 'c', 'd'}));
  

  // Check duplicate adding of edges
  graph.add_edges({{'d', 'c', 'b'}});
  ASSERT_EQ(graph.edge_set('a')[0], std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.edge_set('a').size(), 1);
  ASSERT_TRUE(GMC::contains(graph.edge_set('b'), std::vector<char>({'a', 'c'})));
  ASSERT_TRUE(GMC::contains(graph.edge_set('b'), std::vector<char>({'c', 'd'})));
  ASSERT_EQ(graph.edge_set('b').size(), 2);
  ASSERT_TRUE(GMC::contains(graph.edge_set('c'), std::vector<char>({'a', 'b'})));
  ASSERT_TRUE(GMC::contains(graph.edge_set('c'), std::vector<char>({'b', 'd'})));  
  ASSERT_EQ(graph.edge_set('c').size(), 2);
  ASSERT_TRUE(GMC::contains(graph.edge_set('d'), std::vector<char>({'b', 'c'})));  
  ASSERT_EQ(graph.edge_set('d').size(), 1);

  edges = graph.get_edges();
  ASSERT_EQ(edges.size(), 2);
  ASSERT_EQ(edges[0], std::vector<char>({'a', 'b', 'c'}));
  ASSERT_EQ(edges[1], std::vector<char>({'b', 'c', 'd'}));

  ASSERT_EQ(graph.neighborhood('a'), std::vector<char>({'b', 'c'}));
  ASSERT_EQ(graph.neighborhood('b'), std::vector<char>({'a', 'c', 'd'}));
  ASSERT_EQ(graph.neighborhood('c'), std::vector<char>({'a', 'b', 'd'}));
  ASSERT_EQ(graph.neighborhood('d'), std::vector<char>({'b', 'c'}));

}

TEST(AddEdgeDeathTest, InvalidSize)
{
  GMC::KUniformHypergraph<char> graph(3);

  graph.add_nodes({'a', 'b', 'c'});

  ASSERT_DEATH(graph.add_edge({'a', 'b'}), "");
}

TEST(AddEdgeDeathTest, NotAddedNode)
{
  GMC::KUniformHypergraph<char> graph(3);

  graph.add_nodes({'a', 'b'});

  ASSERT_DEATH(graph.add_edge({'a', 'b', 'c'}), "");
}

TEST(AddEdgesDeathTest, InvalidSizeTest1)
{
  GMC::KUniformHypergraph<char> graph(3);

  graph.add_nodes({'a', 'b', 'c'});

  ASSERT_DEATH(graph.add_edges({{'a', 'b'}}), "");
}

TEST(AddEdgesDeathTest, InvalidSizeTest2)
{
  GMC::KUniformHypergraph<char> graph(3);

  graph.add_nodes({'a', 'b', 'c'});

  ASSERT_DEATH(graph.add_edges({{'a', 'b', 'c'}, {'a', 'b'}}), "");
}


TEST(AddEdgesDeathTest, NotAddedNode)
{
  GMC::KUniformHypergraph<char> graph(3);

  graph.add_nodes({'a', 'b'});

  ASSERT_DEATH(graph.add_edges({{'a', 'b', 'c'}}), "");
}


TEST(GetEdgeDeathTest, Test)
{
  GMC::KUniformHypergraph<char> graph(3);

  ASSERT_DEATH(graph.edge_set('a'), "");
}

TEST(NeighborhoodDeathTest, Test)
{
  GMC::KUniformHypergraph<char> graph(3);

  ASSERT_DEATH(graph.neighborhood('a'), "");
}

TEST(InvalidKDeathTest, Testk1)
{
  ASSERT_DEATH(GMC::KUniformHypergraph<char> graph(1), "");
}

TEST(InvalidKDeathTest, Testk0)
{
  ASSERT_DEATH(GMC::KUniformHypergraph<char> graph(0), "");
}

TEST(InvalidKDeathTest, Testkneg)
{
  ASSERT_DEATH(GMC::KUniformHypergraph<char> graph(-2), "");
}

TEST(K2Test, K2Test)
{
  GMC::KUniformHypergraph<int> graph(2);

  graph.add_nodes({1, 2, 3, 4, 5, 6, 7, 8});
  graph.add_edges({{1, 2}, {2, 3}, {2,4}, {2,5}, {3, 4}, {3, 5}, {4, 5}, {2, 8}, {7, 6}, {6, 3}, {8, 1}});

  ASSERT_EQ(graph.edge_set(1).size(), 2);
  ASSERT_EQ(graph.edge_set(1), std::vector< std::vector<int> >({{2}, {8}}));

  ASSERT_EQ(graph.edge_set(2).size(), 5);
  ASSERT_EQ(graph.edge_set(2), std::vector< std::vector<int> >({{1}, {3}, {4}, {5}, {8}}));
}
