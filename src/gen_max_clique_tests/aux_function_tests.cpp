#include "gen_max_clique/templated_aux_functions.hpp"

#include "gtest/gtest.h"

TEST(CombinationsOfSizeTest, Test1)
{
  std::vector<int> set = {1, 2, 3, 4};
  auto combinations = GMC::combinations_of_size(set, 3);
  GMC::print(std::cout, set);
  std::cout << "\n";  
  GMC::print(std::cout, combinations);
  std::cout << "\n";

  ASSERT_TRUE(GMC::contains(combinations, {1, 2, 3}) ||
              GMC::contains(combinations, {1, 3, 2}) ||
              GMC::contains(combinations, {2, 1, 3}) ||
              GMC::contains(combinations, {2, 3, 1}) ||
              GMC::contains(combinations, {3, 2, 1}) ||
              GMC::contains(combinations, {3, 1, 2}));
  ASSERT_TRUE(GMC::contains(combinations, {1, 2, 4}) ||
              GMC::contains(combinations, {1, 4, 2}) ||
              GMC::contains(combinations, {2, 1, 4}) ||
              GMC::contains(combinations, {2, 4, 1}) ||
              GMC::contains(combinations, {4, 2, 1}) ||
              GMC::contains(combinations, {4, 1, 2}));
  ASSERT_TRUE(GMC::contains(combinations, {4, 2, 3}) ||
              GMC::contains(combinations, {4, 3, 2}) ||
              GMC::contains(combinations, {2, 4, 3}) ||
              GMC::contains(combinations, {2, 3, 4}) ||
              GMC::contains(combinations, {3, 2, 4}) ||
              GMC::contains(combinations, {3, 4, 2}));
  ASSERT_TRUE(GMC::contains(combinations, {1, 4, 3}) ||
              GMC::contains(combinations, {1, 3, 4}) ||
              GMC::contains(combinations, {4, 1, 3}) ||
              GMC::contains(combinations, {4, 3, 1}) ||
              GMC::contains(combinations, {3, 4, 1}) ||
              GMC::contains(combinations, {3, 1, 4}));
  ASSERT_TRUE(combinations.size() == 4);
}

TEST(CombinationsOfSizeTest, SortedTest1)
{
  std::vector<int> set = {1, 2, 3, 4};
  auto combinations = GMC::combinations_of_size(set, 3, true);
  GMC::print(std::cout, set);
  std::cout << "\n";
  GMC::print(std::cout, combinations);
  std::cout << "\n";

  ASSERT_TRUE(combinations.size() == 4);
  ASSERT_TRUE((combinations[0] == std::vector<int>({1, 2, 3})));
  ASSERT_TRUE((combinations[1] == std::vector<int>({1, 2, 4})));  
  ASSERT_TRUE((combinations[2] == std::vector<int>({1, 3, 4})));
  ASSERT_TRUE((combinations[3] == std::vector<int>({2, 3, 4})));
}


TEST(CombinationsOfSizeTest, Test2)
{
  std::vector<char> set = {'a', 'b', 'c', 'd'};
  auto combinations = GMC::combinations_of_size(set, 3);
  GMC::print(std::cout, set);
  std::cout << "\n";  
  GMC::print(std::cout, combinations);
  std::cout << "\n";
  
  ASSERT_TRUE(GMC::contains(combinations, {'a', 'b', 'c'}) ||
              GMC::contains(combinations, {'a', 'c', 'b'}) ||
              GMC::contains(combinations, {'b', 'a', 'c'}) ||
              GMC::contains(combinations, {'b', 'c', 'a'}) ||
              GMC::contains(combinations, {'c', 'b', 'a'}) ||
              GMC::contains(combinations, {'c', 'a', 'b'}));
  ASSERT_TRUE(GMC::contains(combinations, {'a', 'b', 'd'}) ||
              GMC::contains(combinations, {'a', 'd', 'b'}) ||
              GMC::contains(combinations, {'b', 'a', 'd'}) ||
              GMC::contains(combinations, {'b', 'd', 'a'}) ||
              GMC::contains(combinations, {'d', 'b', 'a'}) ||
              GMC::contains(combinations, {'d', 'a', 'b'}));
  ASSERT_TRUE(GMC::contains(combinations, {'d', 'b', 'c'}) ||
              GMC::contains(combinations, {'d', 'c', 'b'}) ||
              GMC::contains(combinations, {'b', 'd', 'c'}) ||
              GMC::contains(combinations, {'b', 'c', 'd'}) ||
              GMC::contains(combinations, {'c', 'b', 'd'}) ||
              GMC::contains(combinations, {'c', 'd', 'b'}));
  ASSERT_TRUE(GMC::contains(combinations, {'a', 'd', 'c'}) ||
              GMC::contains(combinations, {'a', 'c', 'd'}) ||
              GMC::contains(combinations, {'d', 'a', 'c'}) ||
              GMC::contains(combinations, {'d', 'c', 'a'}) ||
              GMC::contains(combinations, {'c', 'd', 'a'}) ||
              GMC::contains(combinations, {'c', 'a', 'd'}));
  ASSERT_TRUE(combinations.size() == 4);
}

TEST(CombinationsOfSizeTest, SortedTest2)
{
  std::vector<char> set = {'a', 'b', 'c', 'd'};
  auto combinations = GMC::combinations_of_size(set, 3, true);
  GMC::print(std::cout, set);
  std::cout << "\n";  
  GMC::print(std::cout, combinations);
  std::cout << "\n";

  
  ASSERT_TRUE(combinations.size() == 4);
  ASSERT_TRUE((combinations[0] == std::vector<char>({'a', 'b', 'c'})));
  ASSERT_TRUE((combinations[1] == std::vector<char>({'a', 'b', 'd'})));  
  ASSERT_TRUE((combinations[2] == std::vector<char>({'a', 'c', 'd'})));
  ASSERT_TRUE((combinations[3] == std::vector<char>({'b', 'c', 'd'})));  
}

TEST(CombinationsOfSizeTest, Test3)
{
  std::vector<char> set = {'a', 'b', 'c', 'd'};
  auto combinations = GMC::combinations_of_size(set, 2);
  GMC::print(std::cout, set);
  std::cout << "\n";  
  GMC::print(std::cout, combinations);
  std::cout << "\n";

  ASSERT_TRUE(GMC::contains(combinations, {'a', 'b'}) ||
              GMC::contains(combinations, {'b', 'a'}) );
  ASSERT_TRUE(GMC::contains(combinations, {'a', 'c'}) ||
              GMC::contains(combinations, {'c', 'a'}) );
  ASSERT_TRUE(GMC::contains(combinations, {'a', 'd'}) ||
              GMC::contains(combinations, {'d', 'a'}) );
  ASSERT_TRUE(GMC::contains(combinations, {'b', 'c'}) ||
              GMC::contains(combinations, {'c', 'b'}) );
  ASSERT_TRUE(GMC::contains(combinations, {'b', 'd'}) ||
              GMC::contains(combinations, {'d', 'b'}) );
  ASSERT_TRUE(GMC::contains(combinations, {'c', 'd'}) ||
              GMC::contains(combinations, {'d', 'c'}) );
  ASSERT_TRUE(combinations.size() == 6);
}

TEST(CombinationsOfSizeTest, SortedTest3)
{
  std::vector<char> set = {'a', 'b', 'c', 'd'};
  auto combinations = GMC::combinations_of_size(set, 2, true);
  GMC::print(std::cout, set);
  std::cout << "\n";  
  GMC::print(std::cout, combinations);
  std::cout << "\n";

  ASSERT_TRUE(combinations.size() == 6);
  ASSERT_TRUE((combinations[0] == std::vector<char>({'a', 'b'})));
  ASSERT_TRUE((combinations[1] == std::vector<char>({'a', 'c'})));
  ASSERT_TRUE((combinations[2] == std::vector<char>({'a', 'd'})));
  ASSERT_TRUE((combinations[3] == std::vector<char>({'b', 'c'})));
  ASSERT_TRUE((combinations[4] == std::vector<char>({'b', 'd'})));
  ASSERT_TRUE((combinations[5] == std::vector<char>({'c', 'd'})));
}

TEST(CombinationsOfSizeTest, Size0Test)
{
  std::vector<char> set = {'a', 'b', 'c', 'd'};
  auto combinations = GMC::combinations_of_size(set, 0, true);
  GMC::print(std::cout, set);
  std::cout << "\n";  
  GMC::print(std::cout, combinations);
  std::cout << "\n";

  ASSERT_EQ(combinations, std::vector< std::vector<char> >({{}}));
}

