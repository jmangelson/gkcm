#include <vector>
#include <iostream>
#include <algorithm>


//////////////////////////////////////
// Auxiliary Functions
//////////////////////////////////////

template<typename T>
void
find_combinations_of_size(std::vector< std::vector<T> >& combinations,
                          std::vector<T> current_partial_combination,
                          std::vector<T> remaining_choices,
                          int size,
                          bool sort)
{
  if (current_partial_combination.size() == size)
  {
    if(sort)
    {
      std::sort(current_partial_combination.begin(),
                current_partial_combination.end());
    }
    combinations.push_back(current_partial_combination);
    return;
  }

  if (remaining_choices.size() + current_partial_combination.size() < size)
  {
    return;
  }

  while(remaining_choices.size() > 0)
  {
    std::vector<T> new_partial_combination = current_partial_combination;
    new_partial_combination.push_back(remaining_choices.back());
    remaining_choices.pop_back();
    find_combinations_of_size(combinations,
                              new_partial_combination,
                              remaining_choices,
                              size,
                              sort);
  }
}

template<typename T>
std::vector< std::vector<T> >
combinations_of_size(std::vector<T> set, int size, bool sort)
{
  std::vector< std::vector<T> > combinations;
  find_combinations_of_size(combinations,
                            std::vector<T>(),
                            set,
                            size,
                            sort);
  if(sort)
  {
    std::sort(combinations.begin(), combinations.end());
  }
  return combinations;
}

template<typename T>
void
print(std::ostream& s, std::vector<T> v)
{
  s << "{";
  for(int i=0;i<v.size();i++)
  {
    s << v[i];
    if(i != v.size()-1)
      s << ", ";
  }
  s << "}";
}

template<typename T>
void print(std::ostream& s, std::vector< std::vector<T> > v)
{
  s << "{";
  for(int i=0;i<v.size();i++)
  {
    print(s, v[i]);
    if(i != v.size()-1)
      s << ", ";
  }
  s << "}";
}
