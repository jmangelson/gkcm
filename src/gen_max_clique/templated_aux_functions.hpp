#ifndef __GMC_AUX_FUNCTIONS
#define __GMC_AUX_FUNCTIONS

#include <vector>
#include <iostream>
#include <algorithm>

namespace GMC
{

////////////////////////
// Auxiliary Functions
////////////////////////

// This function takes a set and a size parameter and
// finds all set.size() choose <size> possible combinations of that set.
//
// The individual combination sets are sorted if sort is true.
template<typename T>
std::vector< std::vector<T> > combinations_of_size(std::vector<T> set, int size, bool sort=false);


// Checks if a vector contains an element
template<typename T>
bool contains(std::vector<T> v, T x, bool sorted=false)
{
  if(v.size() == 0)
  {
    return false;
  }
  if(sorted)
  {
    typename std::vector<T>::iterator it = std::lower_bound(v.begin(), v.end(), x);
    if (it == v.end())
      return false;
    else
      return (x == *it);
  }
  else
  {
    return std::find(v.begin(), v.end(), x) != v.end();
  }
}

// Checks if a vector contains all of the elements in all_x
template<typename T>
bool contains(std::vector<T> v, std::vector<T> all_x, bool sorted=false)
{
  for(auto x : all_x)
  {
    if(!contains(v, x, sorted))
    {
      return false;
    }
  }
  return true;
}

// Prints the contents of a std::vector<T>
template<typename T>
void print(std::ostream& s, std::vector<T> v);

// Prints the contents of a std::vector< std::vector<T> >
template<typename T>
void print(std::ostream& s, std::vector< std::vector<T> > v);

// This funciton inserts a node into a sorted vector only if the
// the vector does not already contain the respective element.
//
// A binary search is used for the insertion and check
template <typename T>
bool unique_insert_into_sorted_vector(std::vector<T>& v, const T& t) {
  typename std::vector<T>::iterator i  = std::lower_bound(v.begin(), v.end(), t);
  if (i == v.end() || t < *i)
  {
    v.insert(i, t);
    return true;
  }
  else
  {
    return false;
  }
}


// The implementation of these functions is included
// here
#include "templated_aux_functions.cpp"

};



#endif //__GMC_AUX_FUNCTIONS
