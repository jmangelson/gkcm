#ifndef __GMC_HPP
#define __GMC_HPP

#include <thread>
#include <mutex>
#include <queue>

#include "templated_aux_functions.hpp"
#include "kuniform_hypergraph.hpp"

namespace GMC
{

///////////////////////////////////////
// Base Class of Max Clique Optimizer
///////////////////////////////////////

template<typename T>
class GeneralizedMaxCliqueSolver
{
 public:
  GeneralizedMaxCliqueSolver() {}
  virtual ~GeneralizedMaxCliqueSolver(){}

  // This function takes a k-uniform hypergraph and estimates or finds
  // its maximum clique
  virtual std::vector<std::vector<T>> solve(KUniformHypergraph<T>& graph) = 0;
  // This function takes a k-uniform hypergraph and a vector of new measurements and will find a new maximum clique
  virtual std::vector<std::vector<T>> solveIncremental(KUniformHypergraph<T> &graph,
                                                       std::vector<T> meas) = 0;
  virtual void clear() = 0;
};


/////////////////////////////////////////
// Generalized Max Clique Exact
/////////////////////////////////////////
// This algorithm implements the exact algorithm presented in:
//
//
//
//
template<typename T>
class MaxCliqueSolverExact : public GeneralizedMaxCliqueSolver<T>
{
  // Private Data Members
 private:
  const int num_threads;
  const int num_cliques;

  std::mutex saved_max_clique_mutex;
  std::vector<std::vector<T>> saved_max_clique; //S_max

  std::mutex nodes_already_searched_mutex;
  std::vector<T> nodes_already_searched;

  std::mutex vertex_queue_mutex;
  std::queue<T> vertex_queue;

  // Constructors
 public:
  MaxCliqueSolverExact(int cliques=1, int threads=1) :
      num_threads(threads),
      num_cliques(cliques),
      saved_max_clique_mutex(),
      saved_max_clique(cliques, std::vector<T>{}),
      nodes_already_searched_mutex(),
      nodes_already_searched(),
      vertex_queue_mutex(),
      vertex_queue()
  {
    assert(this->num_threads > 0);
  }
  ~MaxCliqueSolverExact() {}

  // Public Function Members
 public:
  std::vector<std::vector<T>> solve(KUniformHypergraph<T>& graph)
  {
    return this->max_clique(graph);
  }

  std::vector<std::vector<T>> solveIncremental(KUniformHypergraph<T> &graph,
                                  std::vector<T> meas)
  {
    return this->max_clique_incremental(graph, meas);
  }

  void clear() {
      saved_max_clique.clear();
      nodes_already_searched.clear();
      vertex_queue = {};
  }

  std::vector<std::vector<T>> max_clique(KUniformHypergraph<T>& graph, bool sort=true)
  {
    std::vector<T> empty_vec = {};
    this->saved_max_clique = std::vector<std::vector<int>>(num_cliques,
                                                           empty_vec);

    return this->max_clique(graph, graph.get_nodes(), sort);
  }

  std::vector<std::vector<T>> max_clique_incremental(KUniformHypergraph<T> &graph,
                                        std::vector<T> meas, bool sort=true)
  {
    // Don't want to reset saved_max_clique
    return this->max_clique(graph, meas, sort);
  }

  // Private Function Members
 private:
  std::vector<std::vector<T>> max_clique(KUniformHypergraph<T> &graph,
                                        std::vector<T> meas, bool sort=true)
  {
    this->nodes_already_searched = {};

    if(this->num_threads == 1)
    {
      for(auto v_i : meas)
      {
        max_clique_for_vertex(v_i, graph);
      }
    }
    else
    {
      for(auto v_i : meas)
      {
        this->vertex_queue.push(v_i);
      }

      std::vector<std::thread> threads;
      for(int i=0;i<this->num_threads;i++)
      {
        threads.push_back(std::thread(&MaxCliqueSolverExact::thread_execution, this, std::ref(graph)));
      }

      for(auto& th : threads)
      {
        th.join();
      }
    }

    if(sort)
    {
      for(size_t i{0}; i != saved_max_clique.size(); ++i)
        std::sort(this->saved_max_clique[i].begin(),
                  this->saved_max_clique[i].end());
      std::sort(saved_max_clique.begin(), saved_max_clique.end());
    }

    return this->saved_max_clique;
  }

  void max_clique_for_vertex(T & v_i, KUniformHypergraph<T>& graph)
  {
    //    std::cerr << "v_i: " << v_i << "\n";

    // Read shared information
    int saved_max_clique_size;
    {
      std::lock_guard<std::mutex> guard(this->saved_max_clique_mutex);
      std::vector<T> min_clique;
      min_clique = *std::min_element(saved_max_clique.begin(), saved_max_clique.end(), [](const std::vector<T> &v1, const std::vector<T> &v2) { return v1.size() < v2.size(); });
      saved_max_clique_size = min_clique.size();
    }
    std::vector<T> already_searched;
    {
      std::lock_guard<std::mutex> guard(this->nodes_already_searched_mutex);
      already_searched = this->nodes_already_searched;
    }

    // std::cerr << "Saved max clique size: " << saved_max_clique_size << "\n";
    // std::cerr << "Graph degree(v_i): " << graph.degree(v_i) << "\n";

    // Evaluate possible cliques
    if(graph.degree(v_i) + 1 > saved_max_clique_size)
    {
      for(auto e : graph.edge_set(v_i))
      {
        // std::cerr << "\te: ";
        // print(std::cerr, e);
        // std::cerr << "\n";

        std::vector<T> current_clique = e; //S
        current_clique.push_back(v_i);

        std::vector<T> U;
        std::vector< std::vector<T> > R = combinations_of_size(current_clique,
                                                               graph.k()-1,
                                                               true);
        for(auto v_j : graph.neighborhood(v_i))
        {
          if(!contains(already_searched, v_j, true))
          {
            if(graph.degree(v_j) + 1 >= saved_max_clique_size)
            {
              if(contains(graph.edge_set(v_j), R, true))
              {
                //        std::cerr << "\t\tValid v_j: " << v_j << "\n";
                U.push_back(v_j);
              }
            }
          }
        }
        if(U.size() + current_clique.size() > saved_max_clique_size)
        {
          this->clique(graph, R, current_clique, U);
        }
      }
    }
    else
    {
      //      std::cerr << "\tPruned for small degree\n";
    }

    // Update Searched Nodes
    {
      std::lock_guard<std::mutex> guard(this->nodes_already_searched_mutex);
      unique_insert_into_sorted_vector(this->nodes_already_searched, v_i);
      // std::cerr << "Nodes already Searched:\n";
      // print(std::cerr, this->nodes_already_searched);
      // std::cerr << "\n";
    }
  }

  void thread_execution(KUniformHypergraph<T>& graph)
  {
    T v;

    while(true)
    {
      // Get Vertex From Queue
      {
        std::lock_guard<std::mutex> guard(this->vertex_queue_mutex);

        if(this->vertex_queue.empty())
        {
          return;
        }
        else
        {
          v = this->vertex_queue.front();
          this->vertex_queue.pop();
        }
      }

      // Process Vertex
      this->max_clique_for_vertex(v, graph);
    }
  }

  bool cliquesOverlap(const std::vector<T> &c1, const std::vector<T> &c2) {
    std::vector<T> overlap{};
    std::set_intersection(c1.begin(), c1.end(), c2.begin(), c2.end(),
                          std::back_inserter(overlap));
    if (overlap.size() >=1)
      return true;
    else
      return false;
  }

  void clique(KUniformHypergraph<T>& graph,
              std::vector< std::vector<T> > R,
              std::vector<T> current_clique,
              std::vector<T> U)
  {
    // std::cerr << "\t\t\tClique:\n\t\t\t\tR: ";
    // print(std::cerr, R); std::cerr << "\n\t\t\t\tS: ";
    // print(std::cerr, current_clique); std::cerr << "\n\t\t\t\tU: ";
    // print(std::cerr, U); std::cerr  << "\n";

    if(U.size() == 0)
    {
      std::lock_guard<std::mutex> guard(this->saved_max_clique_mutex);
      auto smallest_max_clique = std::min_element(saved_max_clique.begin(), saved_max_clique.end(), [](const std::vector<T> v1, const std::vector<T> &v2) { return v1.size() < v2.size(); });

      // Are any of the current max cliques a subset of the current clique
      std::sort(current_clique.begin(), current_clique.end());
      for(auto it{saved_max_clique.begin()}; it != saved_max_clique.end(); ++it)
      {
        if(it->size() == 0)
          continue;
        std::sort(it->begin(), it->end());

        bool overlaps = cliquesOverlap(current_clique, *it);
        if(overlaps)
        {
          // keep the biggest
          if(current_clique.size() > it->size())
            (*it) = current_clique;
          return;
        }
      }

      if(current_clique.size() > smallest_max_clique->size())
      {
        bool has_overlap{false};
        for (std::vector<T> c : saved_max_clique)
          has_overlap = cliquesOverlap(c, current_clique);
        if (!has_overlap)
            (*smallest_max_clique) = current_clique;
      }
      return;
    }

    while(U.size() > 0)
    {
      int saved_max_clique_size;
      {
        std::lock_guard<std::mutex> guard(this->saved_max_clique_mutex);
        // saved_max_clique_size = this->saved_max_clique.size();
        std::vector<int> min_clique;
        min_clique = *std::min_element(saved_max_clique.begin(), saved_max_clique.end(), [](const std::vector<T> v1, const std::vector<T> &v2) { return v1.size() < v2.size(); });
        saved_max_clique_size = min_clique.size();
      }
      if(current_clique.size() + U.size() <= saved_max_clique_size)
      {
        return;
      }

      T u = U.back();
      U.pop_back();
      std::vector<T> current_clique_rec = current_clique;
      current_clique_rec.push_back(u);
      std::vector<T> N_prime;
      N_prime.reserve(graph.degree(u));
      for( auto n : graph.neighborhood(u) )
      {
        if(graph.degree(n) >= saved_max_clique_size)
        {
          N_prime.push_back(n);
        }
      }
      std::vector<T> U_rec = {};
      std::vector< std::vector<T> > R_rec = R;
      for( auto p : combinations_of_size(current_clique, graph.k()-2, true))
      {
        p.push_back(u);
        std::sort(p.begin(), p.end());
        R_rec.push_back(p);
      }
      for( auto q : U )
      {
        if(contains(N_prime, q))
        {
          if(contains(graph.edge_set(q), R_rec))
          {
            U_rec.push_back(q);
          }
        }
      }
      this->clique(graph, R_rec, current_clique_rec, U_rec);
    }
  }
};



/////////////////////////////////////////
// Generalized Max Clique Heuristic
/////////////////////////////////////////
// This algorithm implements the exact algorithm presented in:
//
//
//
//
template<typename T>
class MaxCliqueSolverHeuristic : public GeneralizedMaxCliqueSolver<T>
{
  // Private Data Members
 private:
  const int num_threads;
  const int num_cliques;

  std::mutex saved_max_clique_mutex;
  std::vector<std::vector<T>> saved_max_clique; //S_max

  std::mutex vertex_queue_mutex;
  std::queue<T> vertex_queue;

  // Constructors
 public:
  MaxCliqueSolverHeuristic(int cliques=1, int threads=1) :
      num_threads(threads),
      num_cliques(cliques),
      saved_max_clique_mutex(),
      saved_max_clique(cliques, std::vector<T>{}),
      vertex_queue_mutex(),
      vertex_queue()
  {
    assert(this->num_threads > 0);
  }
  ~MaxCliqueSolverHeuristic() {}

  // Public Function Members
 public:
  std::vector<std::vector<T>> solve(KUniformHypergraph<T>& graph)
  {
    return this->max_clique_heu(graph);
  }

  std::vector<std::vector<T>> solveIncremental(KUniformHypergraph<T> &graph,
                                  std::vector<T> meas)
  {
    return this->max_clique_incremental_heu(graph, meas);
  }

  void clear() {
      saved_max_clique.clear();
      vertex_queue = {};
  }

  std::vector<std::vector<T>> max_clique_heu(KUniformHypergraph<T>& graph,
                                bool sort=true)
  {
    std::vector<T> empty_vec = {};
    this->saved_max_clique = std::vector<std::vector<int>>(num_cliques, empty_vec);

    return this->max_clique_heu(graph, graph.get_nodes(), sort);
  }

  std::vector<std::vector<T>> max_clique_incremental_heu(KUniformHypergraph<T> &graph,
                                            std::vector<T> meas,
                                            bool sort=true)
  {
    // Don't reset the saved_max_clique
    return this->max_clique_heu(graph, meas, sort);
  }


  // Private Function Members
 private:

  std::vector<std::vector<T>> max_clique_heu(KUniformHypergraph<T>& graph,
                                const std::vector<T> &meas,
                                bool sort=true)
  {
    if(this->num_threads == 1)
    {
      for(auto v_i : meas)
      {
        max_clique_heu_for_vertex(v_i, graph);
      }
    }
    else
    {
      for(auto v_i : meas)
      {
        this->vertex_queue.push(v_i);
      }

      std::vector<std::thread> threads;
      for(int i=0;i<this->num_threads;i++)
      {
        threads.push_back(std::thread(&MaxCliqueSolverHeuristic::thread_execution, this, std::ref(graph)));
      }

      for(auto& th : threads)
      {
        th.join();
      }
    }

    if(sort)
    {
      for(size_t i{0}; i != saved_max_clique.size(); ++i)
        std::sort(this->saved_max_clique[i].begin(),
                  this->saved_max_clique[i].end());
      std::sort(saved_max_clique.begin(), saved_max_clique.end());
    }

    return this->saved_max_clique;
  }

  void max_clique_heu_for_vertex(T& v_i, KUniformHypergraph<T>& graph)
  {
    int saved_max_clique_size;
    {
      std::lock_guard<std::mutex> guard(this->saved_max_clique_mutex);
      std::vector<T> min_clique;
      min_clique = *std::min_element(saved_max_clique.begin(), saved_max_clique.end(), [](const std::vector<T> &v1, const std::vector<T> &v2) { return v1.size() < v2.size(); });
      saved_max_clique_size = min_clique.size();
    }

    if(graph.degree(v_i) + 1 > saved_max_clique_size)
    {
      std::map<T, int> num_connections;

      for(auto n : graph.neighborhood(v_i))
      {
        num_connections[n] = 0;
      }

      for(auto e : graph.edge_set(v_i))
      {
        for(auto n : e)
        {
          num_connections[n]++;
        }
      }

      int max_num_connections = 0;
      std::vector<T> max_edge;
      for (auto e : graph.edge_set(v_i))
      {
        int total_connections = 0;
        for(auto n : e)
        {
          total_connections += num_connections[n];
        }
        if(total_connections > max_num_connections)
        {
          max_edge = e;
          max_num_connections = total_connections;
        }
      }

      std::vector<T> current_clique = max_edge;
      current_clique.push_back(v_i);

      //std::cout << "Initial Clique: ";
      //        print(std::cout, current_clique);
      //        std::cout << "\n";

      std::vector<T> U;
      std::vector< std::vector<T> > R = combinations_of_size(current_clique,
                                                             graph.k()-1,
                                                             true);
      for(auto v_j : graph.neighborhood(v_i))
      {
        if(graph.degree(v_j) + 1 >= saved_max_clique_size)
        {
          if(contains(graph.edge_set(v_j), R, true))
          {
            //                std::cerr << "\t\tValid v_j: " << v_j << "\n";
            U.push_back(v_j);
          }
        }
      }
      if(U.size() + current_clique.size() > saved_max_clique_size)
      {
        this->clique_heu(graph, R, current_clique, U, num_connections);
      }

    }
    else
    {
      //        std::cerr << "\tPruned for small degree\n";
    }
  }

  void thread_execution(KUniformHypergraph<T>& graph)
  {
    T v;

    while(true)
    {
      // Get Vertex From Queue
      {
        std::lock_guard<std::mutex> guard(this->vertex_queue_mutex);

        if(this->vertex_queue.empty())
        {
          return;
        }
        else
        {
          v = this->vertex_queue.front();
          this->vertex_queue.pop();
        }
      }

      // Process Vertex
      this->max_clique_heu_for_vertex(v, graph);
    }
  }

  class DegreeSort {
   private:
    KUniformHypergraph<T>& graph_;

   public:
    DegreeSort(KUniformHypergraph<T>& graph):
        graph_(graph)
    {}
    ~DegreeSort() {}

    bool operator() (T node_i, T  node_j)
    {
      return (graph_.degree(node_i) < graph_.degree(node_j));
    }
  };

  class ConnectionSort {
   private:
    std::map<T, int>& connection_map_;

   public:
    ConnectionSort(std::map<T, int>& connection_map):
        connection_map_(connection_map)
    {}
    ~ConnectionSort() {}

    bool operator() (T node_i, T  node_j)
    {
      return (connection_map_[node_i] < connection_map_[node_j]);
    }
  };

  bool cliquesOverlap(const std::vector<T> &c1, const std::vector<T> &c2) {
    std::vector<T> overlap{};
    std::set_intersection(c1.begin(), c1.end(), c2.begin(), c2.end(),
                          std::back_inserter(overlap));
    if (overlap.size() >=1)
      return true;
    else
      return false;
  }

  void clique_heu(KUniformHypergraph<T>& graph,
                   std::vector< std::vector<T> > R,
                   std::vector<T> current_clique,
                   std::vector<T> U,
                   std::map<T, int>& connection_map)
  {
    //       std::cerr << "\t\t\tClique:\n\t\t\t\tR: ";
    //       print(std::cerr, R); std::cerr << "\n\t\t\t\tS: ";
    //       print(std::cerr, current_clique); std::cerr << "\n\t\t\t\tU: ";
    //       print(std::cerr, U); std::cerr  << "\n";

    if(U.size() == 0)
    {
      std::lock_guard<std::mutex> guard(this->saved_max_clique_mutex);
      auto smallest_max_clique = std::min_element(saved_max_clique.begin(), saved_max_clique.end(), [](const std::vector<T> v1, const std::vector<T> &v2) { return v1.size() < v2.size(); });

      // Are any of the current max cliques a subset of the current clique
      std::sort(current_clique.begin(), current_clique.end());


      for(auto it{saved_max_clique.begin()}; it != saved_max_clique.end(); ++it)
      {
        if(it->size() == 0)
          continue;
        std::sort(it->begin(), it->end());

        bool overlaps = cliquesOverlap(current_clique, *it);
        if(overlaps)
        {
          // keep the biggest
          if(current_clique.size() > it->size())
            (*it) = current_clique;
          return;
        }
      }

      if(current_clique.size() > smallest_max_clique->size())
      {
        bool has_overlap{false};
        for (std::vector<T> c : saved_max_clique) {
          has_overlap = cliquesOverlap(c, current_clique);
          if (has_overlap)
            break;
        }
        if (!has_overlap)
            (*smallest_max_clique) = current_clique;
      }
      return;
    }

    int saved_max_clique_size;
    {
      std::lock_guard<std::mutex> guard(this->saved_max_clique_mutex);
      std::vector<int> min_clique;
      min_clique = *std::min_element(saved_max_clique.begin(), saved_max_clique.end(), [](const std::vector<T> v1, const std::vector<T> &v2) { return v1.size() < v2.size(); });
      saved_max_clique_size = min_clique.size();
    }

    // Get u with highest degree
    //    DegreeSort comparator(graph);
    // ConnectionSort comparator(connection_map);

    // std::sort(U.begin(),
    //           U.end(),
    //           comparator);
    // T u = U.back();
    // U.pop_back();




    T u_max = U[0];

    for(auto u_tmp : U)
    {
      if(connection_map[u_tmp] > connection_map[u_max])
      {
        u_max = u_tmp;
      }
    }

    T u = u_max;
    U.erase(std::remove(U.begin(), U.end(), u_max), U.end());

    //    std::cerr << "\n\t\t\t\tSelected u: " << u << " with connection num: " << connection_map[u] << "\n";

    std::vector<T> current_clique_rec = current_clique;
    current_clique_rec.push_back(u);

    // Calculate N_prime
    std::vector<T> N_prime;
    N_prime.reserve(graph.degree(u));
    for( auto n : graph.neighborhood(u) )
    {
      if(graph.degree(n) >= saved_max_clique_size)
      {
        unique_insert_into_sorted_vector(N_prime, n);
        //        N_prime.push_back(n);
      }
    }

    // Determine R_rec and U_rec
    std::vector<T> U_rec = {};
    std::vector< std::vector<T> > R_rec = R;
    for( auto p : combinations_of_size(current_clique, graph.k()-2, true))
    {
      p.push_back(u);
      std::sort(p.begin(), p.end());
      unique_insert_into_sorted_vector(R_rec, p);
      //      R_rec.push_back(p);
    }
    for( auto q : U )
    {
      if(contains(N_prime, q, true))
      {
        if(contains(graph.edge_set(q), R_rec, true))
        {
          U_rec.push_back(q);
        }
      }
    }

    // Recursively call clique_heu
    this->clique_heu(graph, R_rec, current_clique_rec, U_rec, connection_map);
  }
};


} // namespace
#endif //__GMC_HPP
