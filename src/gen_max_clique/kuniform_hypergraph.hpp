#ifndef __GMC_KUNIFORM_HYPERGRAPH
#define __GMC_KUNIFORM_HYPERGRAPH

#undef NDEBUG
#include <assert.h>
#include <vector>
#include <set>
#include <map>

#include "templated_aux_functions.hpp"

namespace GMC
{

///////////////////////////////////////
// k-Uniform Hypergraph Storage Class
///////////////////////////////////////
template<typename T>
class KUniformHypergraph
{
  // Private Data Members
 private:
  const int k_;

  std::vector<T> added_nodes_;
  std::vector< std::vector<T> > added_edges_;

  std::map< T, std::vector< T > > neighborhoods_;
  std::map< T, std::vector< std::vector<T> > >edge_sets_;

  // Constructors
 public:
  // Constructs a k-uniform hypergraph with k-tuple edges
  //
  // k is required to be greater than or equal to 2
  KUniformHypergraph(int k)
      : k_(k),
        added_nodes_(),
        added_edges_(),
        neighborhoods_(),
        edge_sets_()
  {
    assert(k >= 2);
  }
  ~KUniformHypergraph(){}

  //Public Function Members
 public:

  // Accessor Functions
  int k() {return this->k_;}
  bool contains_node(T node){ return contains(this->added_nodes_, node); }
  int num_nodes() { return this->added_nodes_.size(); }
  std::vector< T > get_nodes() {return this->added_nodes_;}
  std::vector< std::vector<T> > get_edges() {return this->added_edges_;}

  void clear() {
      added_nodes_.clear();
      added_edges_.clear();
      neighborhoods_.clear();
      edge_sets_.clear();
  }

  int degree(T node)
  {
    assert(contains(this->added_nodes_, node));
    return this->neighborhoods_[node].size();
  }

  std::vector< T > neighborhood(T node)
  {
    assert(contains(this->added_nodes_, node));
    return this->neighborhoods_[node];
  }

  std::vector< std::vector< T > > edge_set(T node)
  {
    assert(contains(this->added_nodes_, node));
    return this->edge_sets_[node];
  }


  // This function adds a new edge to the graph
  //
  // This requires that the relevent nodes already be added to the graph
  void add_edge(std::vector<T> edge)
  {
    assert(edge.size() == this->k_);

    for(auto n : edge)
    {
      assert(contains(this->added_nodes_, n, true));
    }

    auto sorted_edge = edge;
    std::sort(sorted_edge.begin(), sorted_edge.end());

    unique_insert_into_sorted_vector< std::vector<T> >(this->added_edges_,
                                                       sorted_edge);

    for(int i=0;i<edge.size();i++)
    {
      auto new_edge = edge;
      new_edge.erase(new_edge.begin()+i);
      std::sort(new_edge.begin(), new_edge.end());

      if(unique_insert_into_sorted_vector< std::vector<T> >(this->edge_sets_[edge[i]],
                                                            new_edge))
      {
        for(auto n : new_edge)
        {
          unique_insert_into_sorted_vector< T >(this->neighborhoods_[edge[i]],
                                                             n);
        }
      }
    }
  }

  // This function adds a set of new edges to the graph
  //
  // This requires that the relevent nodes already be added to the graph
  //
  // This function is much more efficient than add_edge for large numbers of edges and nodes.
  void add_edges(std::vector< std::vector<T> > edges)
  {
    std::set<T> touched_nodes;

    for( auto edge : edges )
    {
      assert(edge.size() == this->k_);

      for(auto n : edge)
      {
        touched_nodes.insert(n);
      }
    }

    for(auto n : touched_nodes)
    {
      assert(contains(this->added_nodes_, n, true));
    }

    std::map<T, std::set< std::vector<T> > > copied_edge_sets;
    std::map<T, std::set<T> > copied_neighborhoods;

    for( auto node : touched_nodes )
    {
      std::copy(this->edge_sets_[node].begin(),
                this->edge_sets_[node].end(),
                std::inserter(copied_edge_sets[node], copied_edge_sets[node].begin()));

      std::copy(this->neighborhoods_[node].begin(),
                this->neighborhoods_[node].end(),
                std::inserter(copied_neighborhoods[node], copied_neighborhoods[node].begin()));
    }

    std::set< std::vector<T> > copied_added_edges;
    std::copy(this->added_edges_.begin(),
              this->added_edges_.end(),
              std::inserter(copied_added_edges, copied_added_edges.begin()));

    //    int num_edges = 0;

    for( auto edge : edges)
    {
      //      num_edges++;
      //      if(num_edges % 1000 == 0)
      //      {
      //        std::cout << num_edges << "\n";
      //      }

      auto sorted_edge = edge;
      std::sort(sorted_edge.begin(), sorted_edge.end());

      copied_added_edges.insert(sorted_edge);

      for(int i=0;i<edge.size();i++)
      {
        auto new_edge = edge;
        new_edge.erase(new_edge.begin()+i);
        std::sort(new_edge.begin(), new_edge.end());

        auto result = copied_edge_sets[edge[i]].insert(new_edge);
        if(result.second)
        {
          for(auto n : new_edge)
          {
            copied_neighborhoods[edge[i]].insert(n);
          }
        }
      }
    }

    for( auto node : touched_nodes )
    {
      this->edge_sets_[node].clear();
      this->edge_sets_[node].reserve(copied_edge_sets[node].size());
      std::copy(copied_edge_sets[node].begin(),
                copied_edge_sets[node].end(),
                std::back_inserter(this->edge_sets_[node]));

      this->neighborhoods_[node].clear();
      this->neighborhoods_[node].reserve(copied_neighborhoods[node].size());
      std::copy(copied_neighborhoods[node].begin(),
                copied_neighborhoods[node].end(),
                std::back_inserter(this->neighborhoods_[node]));
    }

    this->added_edges_.clear();
    this->added_edges_.reserve(copied_added_edges.size());
    std::copy(copied_added_edges.begin(),
              copied_added_edges.end(),
              std::back_inserter(this->added_edges_));
  }

  // This function adds a new node to the graph
  void add_node(T node)
  {
    assert(unique_insert_into_sorted_vector< T >(this->added_nodes_,
                                                 node));

    this->neighborhoods_[node] = {};
  }


  // This function adds new nodes to the graph
  void add_nodes(std::vector< T > nodes)
  {
    for(auto n : nodes)
    {
      assert(unique_insert_into_sorted_vector< T >(this->added_nodes_,
                                                   n));
      this->neighborhoods_[n] = {};
    }
  }

  void print(std::ostream& s)
  {
    s << " Nodes:\n";
    GMC::print(s, this->added_nodes_);
    s << "\nEdges:\n";
    GMC::print(s, this->added_edges_);
    s << "\n";
  }

};

} // namespace GMC

#endif //__GMC_KUNIFORM_HYPERGRAPH
