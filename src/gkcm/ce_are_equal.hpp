#ifndef __GKCM_CE_ARE_EQUAL_HPP
#define __GKCM_CE_ARE_EQUAL_HPP

#include "gkcm.hpp"

namespace GkCM
{

template<class CombT>
class ConsistentIfEqualFunctor
{
 public:
  bool operator ()(const CombT& comb)
  {
    int k = CombT::k;
    typename CombT::MeasType meas = comb.get_meas(0);
    for(int i=1;i<k;i++)
    {
      typename CombT::MeasType next_meas = comb.get_meas(i);
      if(meas.get_value() != next_meas.get_value())
        return false;
      meas = next_meas;
    }
    return true;
  }
};

template<typename DataT, int K>
class AreEqualConsistencyEvaluator :
      public ConsistencyEvaluator<Measurement<DataT>,
                                  Combination<Measurement<DataT>, K>,
                                  ConsistentIfEqualFunctor< Combination<Measurement<DataT>, K> > >
{
 public:
  AreEqualConsistencyEvaluator() {}
  ~AreEqualConsistencyEvaluator() {}
};

                                  

} // namespace GkCM

#endif // __GKCM_CE_ARE_EQUAL_HPP
