#include "gkcm.hpp"
#include <vector>
#include <algorithm>

unsigned GkCM::n_choose_k( unsigned n, unsigned k )
{
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    int result = n;
    for( int i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}

std::vector<std::vector<int>> GkCM::find_combinations_that_need_checked(
                                std::vector<int> indices, int k1, int k2,
                                std::vector<std::vector<int>> combinations) {
    // Augment previously consistent sets with new meausrements
    std::vector<std::vector<int>> combs{};
    std::vector<int> partial_indices{};
    for (std::vector<int> partial_comb : combinations) {
        // remove elements in indices that are also in partial comb
        partial_indices = indices;
        auto it = std::remove_if(partial_indices.begin(), partial_indices.end(),
                [partial_comb](int x) -> bool {
                    return std::find(partial_comb.begin(), partial_comb.end(), x) != partial_comb.end();
                    });
        partial_indices.resize(std::distance(partial_indices.begin(), it));
        GMC::find_combinations_of_size(combs, partial_comb, partial_indices, k2, true);
        std::sort(combs.begin(), combs.end());
        auto ip = std::unique(combs.begin(), combs.end());
        combs.resize(std::distance(combs.begin(), ip));
    }

    return combs;
}
