#ifndef __GKCM_HPP
#define __GKCM_HPP

#undef NDEBUG
#include <assert.h>
#include <array>

#include "gen_max_clique/gmc.hpp"

namespace GkCM
{

///////////////////////////////////////////
// Auxiliary Functions
///////////////////////////////////////////
unsigned n_choose_k( unsigned n, unsigned k );

std::vector<std::vector<int>> find_combinations_that_need_checked(
                          std::vector<int> indices, int k1, int k2,
                          std::vector<std::vector<int>> combinations);

/////////////////////////////////
// Measurement Template
/////////////////////////////////
template<typename DataT>
class Measurement
{
 protected:
  DataT value;

 public:
  Measurement()
      : value()
  {}

  Measurement(DataT val)
      : value(val)
  {}

  Measurement(const Measurement& other)
      : value(other.get_value())
  {}

  DataT get_value() const {return value;}
};

//////////////////////////////////
// Combination Template
//////////////////////////////////
template<typename MeasT, int K>
class Combination
{
 public:
  typedef MeasT MeasType;
  static const int k = K;

 protected:
  std::array<MeasT, K> values;

 public:
  Combination(std::vector<MeasT> measurements)
      : values()
  {
    assert(measurements.size() == k);
    for(int i=0;i<k;i++)
    {
      values[i] = measurements[i];
    }
  }

  Combination(std::vector<int> indices,
              const std::vector<MeasT>& measurement_map)
      : values()
  {
    assert(indices.size() == k);
    for(int i=0;i<k;i++)
    {
      values[i] = measurement_map[indices[i]];
    }
  }

  Combination(const Combination& other)
      : values(other.get_all_meas())
  {}

  std::array<MeasT, K> get_all_meas() const {return values;}
  MeasT get_meas(int i) const {return values[i];}
};

/////////////////////////////////////////
// Consistency Evaluator Template
/////////////////////////////////////////
template<class MeasurementT, class CombinationT, class EvaluationFunctorT>
class ConsistencyEvaluator
{
 public:
  typedef MeasurementT MeasT;
  typedef CombinationT CombT;
  typedef EvaluationFunctorT FunctorT;

 protected:
  const int k_;

 public:
  ConsistencyEvaluator()
      : k_(CombT::k)
  {
    assert(k_ >= 2);
  }
  virtual ~ConsistencyEvaluator(){}

 public:
  int k() const {return this->k_;}

  // // This function should return the consistency of
  // // a set of k measurements, specified by measurement map index
  // virtual bool evaluate_consistency(std::vector<int> measurements,
  //                                   const std::vector<MeasurementT>& measurement_map) = 0;

  virtual std::vector< std::vector<int> >
  evaluate_consistency_with_all_measurements(
      std::vector<int> measurements_to_check,
      const std::vector<MeasurementT>& measurement_map,
      bool sort=false)
  {
    if (measurement_map.size() < this->k())
      return std::vector<std::vector<int>>{};
    // Create vector to store consistent groups
    std::vector< std::vector<int> > consistent_groups;
    consistent_groups.reserve( n_choose_k(measurement_map.size(), this->k_-1) *
                               measurements_to_check.size());

    // Determine what combinations to check
    auto combinations_to_check = find_combinations_that_need_checked(
        measurements_to_check,
        measurement_map,
        sort);

    // Compile the Data necessary for each combination
    std::vector<CombinationT> combinations;
    combinations.reserve(combinations_to_check.size());
    for(std::vector<int> comb : combinations_to_check)
    {
      CombinationT combination_data(comb,measurement_map);
      combinations.push_back(combination_data);
    }

    // Evaluate the Results
    std::vector<bool> results =
        evaluate_consistency_of_combinations(combinations);

    // Reformat and return the results
    for(int i=0;i<results.size();i++)
    {
      if(results[i])
      {
        consistent_groups.push_back(combinations_to_check[i]);
      }
    }
    return consistent_groups;
  }

  virtual std::vector<bool>
  evaluate_consistency_of_combinations(std::vector<CombinationT> combinations)
  {
    EvaluationFunctorT eval;

    std::vector<bool> are_consistent;
    are_consistent.reserve(combinations.size());

    for(auto comb : combinations)
    {
      are_consistent.push_back(eval(comb));
    }

    return are_consistent;
  }

//  private:

  //////////////////////////////////////////////////////////////
  // This function determines the combinations that need to
  // be checked for consistency for a given set of measurements
  // who's consistency needs to be evaluated with each other
  // and all other measurements in the measurement map
  std::vector< std::vector<int> >
  find_combinations_that_need_checked(
      std::vector<int> measurements_to_check,
      const std::vector<MeasurementT>& measurement_map,
      bool sort=false)
  {
    std::vector< std::vector<int> > combinations_to_check;
    combinations_to_check.reserve( n_choose_k(measurement_map.size(), this->k_-1) *
                                   measurements_to_check.size());

    while(!measurements_to_check.empty())
    {
      int measurement = measurements_to_check.back();
      measurements_to_check.pop_back();
      this->find_groups_that_need_checked_impl(measurement,
                                               measurement_map.size(),
                                               measurements_to_check,
                                               combinations_to_check,
                                               sort);
    }

    combinations_to_check.shrink_to_fit();

    return combinations_to_check;
  }

 private:

  // Auxilliary funcition that helps find the combinations
  // that need checked
  void find_groups_that_need_checked_impl(
      int measurement,
      int total_num_measurements,
      std::vector<int> exceptions,
      std::vector< std::vector<int> >& combinations_to_check,
      bool sort=false)
  {
    std::sort(exceptions.begin(), exceptions.end());
    std::vector<int> measurements_to_check;
    measurements_to_check.reserve(total_num_measurements);
    for(int i=0;i<total_num_measurements;i++)
    {
      if(!GMC::contains(exceptions, i, true) &&
         i != measurement)
      {
        measurements_to_check.push_back(i);
      }
    }

    auto combinations_to_check_local = GMC::combinations_of_size(
        measurements_to_check,
        this->k_-1,
        true);

    for(auto combination : combinations_to_check_local)
    {
      combination.push_back(measurement);
      if(sort)
      {
        std::sort(combination.begin(), combination.end());
      }

      combinations_to_check.push_back(combination);
    }
  }
};

/////////////////////////////////////////
// GkCM Solver
/////////////////////////////////////////

// The template parameters specify the cardinality of the edges in the graph,
// the measurement type,
// and the function that will be used to evaluate consistency.
//
// k specifies the cardinality of the edges in the graph
//
// The MeasurementT object stores necessary information about the measurement.
//
// The ConsistencyEvaluatorT object must be callable with two integers
// specifying the indices of two measurements and return a bool.
// It should also be callable with a single integer (i) and return a vector
// of bools with length i-1 specifying the consistency of the measurement
// i with all measurements before it.
template<int K,
         class MeasurementT,
         class ConsistencyEvaluatorT>
class GkCMSolver
{
  // Data Members
 private:
  GMC::KUniformHypergraph<int> graph_;

  GMC::GeneralizedMaxCliqueSolver<int>& mcs_;

  ConsistencyEvaluatorT& consistency_evaluator_;

  std::vector< MeasurementT > measurements_;

  // Function Members
 public:
  GkCMSolver(GMC::GeneralizedMaxCliqueSolver<int>& mcs,
            ConsistencyEvaluatorT& consistency_evaluator):
      graph_(K),
      mcs_(mcs),
      consistency_evaluator_(consistency_evaluator),
      measurements_()
  {
    static_assert(
        std::is_same<MeasurementT,typename ConsistencyEvaluatorT::MeasT>::value,
        "MeasurementT and ConsistencyEvaluatorT:MeasT are not the same");
  }

  std::vector<std::vector<int>> solve_pcm()
  {
    return this->mcs_.solve(this->graph_);
  }

  std::vector<std::vector<int>> solve()
  {
    return this->mcs_.solve(this->graph_);
  }

  std::vector<std::vector<int>> solve_incremental(std::vector<int> measurements) {
    return this->mcs_.solveIncremental(this->graph_, measurements);
  }

  void clear() {
      measurements_.clear();
      mcs_.clear();
      graph_.clear();
  }

  void add_measurements(std::vector<MeasurementT> measurements)
  {
    std::vector<int> indices;
    indices.reserve(measurements.size());
    for( auto m : measurements )
    {
      indices.push_back(this->measurements_.size());
      this->graph_.add_node(this->measurements_.size());
      this->measurements_.push_back(m);
    }

    auto consistent_sets =
        this->consistency_evaluator_.evaluate_consistency_with_all_measurements(
            indices,
            this->measurements_);



    this->graph_.add_edges(consistent_sets);
  }

  // Accessors
  int total_num_measurements() { return this->measurements_.size(); }

  const std::vector< MeasurementT >& measurement_map() { return this->measurements_; }

  void print_graph()
  {
    this->graph_.print(std::cout);
  }

  void replace_measurement(MeasurementT meas, int idx) {
    measurements_[idx] = meas;
  }

  std::vector<std::vector<int>> get_consistent_measurements()
  {
    std::vector<std::vector<int>> edges = this->graph_.get_edges();
    std::sort(edges.begin(), edges.end());
    return edges;
  }
};

/*
These class are for a heirarchy of consistency checks For all the below the templates ore the following
    K: degree of consistency check
    MeasurementT: Class of the measurement type
    ConsistencyEvaluatorT: Consistency we wish to check
    ConsistencyEvaluatorQ: Check used before ConsistencyEvaluatorT

    ConsistencyEvaluatorQ.k() < K!!!!
*/
template<int K,
         class MeasurementT,
         class ConsistencyEvaluatorT,
         class PreEvaluator>
class GkCMHierarchySolver2
{
  // Data Members
 private:
  GMC::KUniformHypergraph<int> graph_;

  GMC::GeneralizedMaxCliqueSolver<int>& mcs_;

  ConsistencyEvaluatorT& consistency_evaluator_;
  PreEvaluator& pre_evaluator_;  // Used to reduce the number of checks

  std::vector< MeasurementT> measurements_;
  GMC::KUniformHypergraph<int> hierarchy_graph_;

  // Function Members
 public:
  GkCMHierarchySolver2(GMC::GeneralizedMaxCliqueSolver<int>& mcs,
            ConsistencyEvaluatorT& consistency_evaluator,
            PreEvaluator& pre_evaluator):
      graph_(K),
      mcs_(mcs),
      consistency_evaluator_(consistency_evaluator),
      pre_evaluator_(pre_evaluator),
      measurements_(),
      hierarchy_graph_(pre_evaluator.k()) {
    static_assert(
        std::is_same<MeasurementT,typename ConsistencyEvaluatorT::MeasT>::value,
        "MeasurementT and ConsistencyEvaluatorT:MeasT are not the same");
    assert(pre_evaluator_.k() < consistency_evaluator_.k());
  }

  std::vector<std::vector<int>> solve_pcm()
  {
    return this->mcs_.solve(this->graph_);
  }

  std::vector<std::vector<int>> solve()
  {
    return this->mcs_.solve(this->graph_);
  }

  std::vector<std::vector<int>> solve_incremental(std::vector<int> measurements) {
    return this->mcs_.solveIncremental(this->graph_, measurements);
  }

  void clear() {
      measurements_.clear();
      mcs_.clear();
      graph_.clear();
  }

  void add_measurements(std::vector<MeasurementT> measurements)
  {
    std::vector<int> indices;
    indices.reserve(measurements.size());
    for( auto m : measurements )
    {
      indices.push_back(this->measurements_.size());
      this->graph_.add_node(this->measurements_.size());
      this->hierarchy_graph_.add_node(this->measurements_.size());
      this->measurements_.push_back(m);
    }

    auto consistent_sets =
        this->pre_evaluator_.evaluate_consistency_with_all_measurements(
                             indices,
                             this->measurements_);

    this->hierarchy_graph_.add_edges(consistent_sets);
    // All edges in this graph are potentially group-k consistent
    consistent_sets = this->hierarchy_graph_.get_edges();

    if (consistent_sets.size() < 1)
      return;

    // Only need to check combinations that include at least 1 new measurement
    std::vector<std::vector<int>> sets_to_check = find_combinations_that_need_checked(indices, pre_evaluator_.k(), K,
                                            consistent_sets);

    std::vector<typename ConsistencyEvaluatorT::CombT> combinations{};
    std::vector<MeasurementT> meas_comb{};
    for (std::vector<int> set : sets_to_check) {
      meas_comb.clear();
      for (int i : set)
        meas_comb.push_back(this->measurements_[i]);
      combinations.emplace_back(meas_comb);
    }

    std::vector<bool> is_consistent = this->consistency_evaluator_.evaluate_consistency_of_combinations(combinations);

    for (int i{0}; i != is_consistent.size(); ++i) {
      if (is_consistent[i]) {
        this->graph_.add_edge(sets_to_check[i]);
      }
    }
  }

  // Accessors
  int total_num_measurements() { return this->measurements_.size(); }

  const std::vector< MeasurementT >& measurement_map() { return this->measurements_; }

  void print_graph()
  {
    this->graph_.print(std::cout);
  }

  void replace_measurement(MeasurementT meas, int idx) {
    measurements_[idx] = meas;
  }

  std::vector<std::vector<int>> get_consistent_measurements()
  {
    std::vector<std::vector<int>> edges = this->graph_.get_edges();
    std::sort(edges.begin(), edges.end());
    return edges;
  }
};

/*
These class are for a heirarchy of consistency checks For all the below the templates ore the following
    K: degree of consistency check
    MeasurementT: Class of the measurement type
    ConsistencyEvaluatorT: Consistency we wish to check
    PreEvaluator1: Check used before PreEvaluator2
    PreEvaluator2: Check used before ConsistencyEvaluatorT

    PreEvaluator1.k() < PreEvaluator2.k()!!!!
    PreEvaluator2.k() < K
*/
template<int K,
         class MeasurementT,
         class ConsistencyEvaluatorT,
         class PreEvaluator1,
         class PreEvaluator2>
class GkCMHierarchySolver3
{
  // Data Members
 private:
  GMC::KUniformHypergraph<int> graph_;

  GMC::GeneralizedMaxCliqueSolver<int>& mcs_;

  ConsistencyEvaluatorT& consistency_evaluator_;
  PreEvaluator1& pre_evaluator1_;
  PreEvaluator2& pre_evaluator2_;

  std::vector< MeasurementT> measurements_;
  std::vector<GMC::KUniformHypergraph<int>> hierarchy_graphs_;

  // Function Members
 public:
  GkCMHierarchySolver3(GMC::GeneralizedMaxCliqueSolver<int>& mcs,
            ConsistencyEvaluatorT& consistency_evaluator,
            PreEvaluator1& pre_evaluator1,
            PreEvaluator2& pre_evaluator2):
      graph_(K),
      mcs_(mcs),
      consistency_evaluator_(consistency_evaluator),
      pre_evaluator1_(pre_evaluator1),
      pre_evaluator2_(pre_evaluator2),
      measurements_(),
      hierarchy_graphs_{} {
    static_assert(
        std::is_same<MeasurementT,typename ConsistencyEvaluatorT::MeasT>::value,
        "MeasurementT and ConsistencyEvaluatorT:MeasT are not the same");
    assert(pre_evaluator1.k() < pre_evaluator2.k());
    assert(pre_evaluator2.k() < consistency_evaluator_.k());
    hierarchy_graphs_.push_back(GMC::KUniformHypergraph<int>(pre_evaluator1.k()));
    hierarchy_graphs_.push_back(GMC::KUniformHypergraph<int>(pre_evaluator2.k()));
  }

  std::vector<std::vector<int>> solve_pcm()
  {
    return this->mcs_.solve(this->graph_);
  }

  std::vector<std::vector<int>> solve()
  {
    return this->mcs_.solve(this->graph_);
  }

  std::vector<std::vector<int>> solve_incremental(std::vector<int> measurements) {
    return this->mcs_.solveIncremental(this->graph_, measurements);
  }

  void clear() {
      measurements_.clear();
      mcs_.clear();
      graph_.clear();
  }

  void add_measurements(std::vector<MeasurementT> measurements)
  {
    std::vector<int> indices;
    indices.reserve(measurements.size());
    for ( auto m : measurements ) {
      indices.push_back(this->measurements_.size());
      this->graph_.add_node(this->measurements_.size());
      for (auto &graph : this->hierarchy_graphs_)
        graph.add_node(this->measurements_.size());
      this->measurements_.push_back(m);
    }

    auto consistent_sets =
        this->pre_evaluator1_.evaluate_consistency_with_all_measurements(
                             indices,
                             this->measurements_);

    this->hierarchy_graphs_[0].add_edges(consistent_sets);
    consistent_sets = this->hierarchy_graphs_[0].get_edges();

    checkConsistency(pre_evaluator2_, this->hierarchy_graphs_[1], indices,
                     this->hierarchy_graphs_[0].get_edges());

    checkConsistency(consistency_evaluator_, graph_, indices,
                     this->hierarchy_graphs_[1].get_edges());
  }

  // Accessors
  int total_num_measurements() { return this->measurements_.size(); }

  const std::vector< MeasurementT >& measurement_map() { return this->measurements_; }

  void print_graph()
  {
    this->graph_.print(std::cout);
  }

  void replace_measurement(MeasurementT meas, int idx) {
    measurements_[idx] = meas;
  }

  std::vector<std::vector<int>> get_consistent_measurements()
  {
    std::vector<std::vector<int>> edges = this->graph_.get_edges();
    std::sort(edges.begin(), edges.end());
    return edges;
  }

 private:
  template<class EvalT>
  void checkConsistency(EvalT &evaluator,
                        GMC::KUniformHypergraph<int> &graph,
                        std::vector<int> indices,
                        std::vector<std::vector<int>> consistent_sets) {
    if (consistent_sets.size() < 1)
      return;
    std::vector<std::vector<int>> sets_to_check =
        find_combinations_that_need_checked(indices, consistent_sets[0].size(),
                                            evaluator.k(), consistent_sets);

    std::vector<typename EvalT::CombT> combinations{};
    std::vector<MeasurementT> meas_comb{};
    for (std::vector<int> set : sets_to_check) {
      meas_comb.clear();
      for (int i : set)
        meas_comb.push_back(this->measurements_[i]);
      combinations.emplace_back(meas_comb);
    }

    std::vector<bool> is_consistent =
        evaluator.evaluate_consistency_of_combinations(combinations);
    for (int i{0}; i != is_consistent.size(); ++i) {
      if (is_consistent[i]) {
        graph.add_edge(sets_to_check[i]);
      }
    }
  }
};

/*
These class are for a heirarchy of consistency checks For all the below the templates ore the following
    K: degree of consistency check
    MeasurementT: Class of the measurement type
    ConsistencyEvaluatorT: Consistency we wish to check
    PreEvaluator1: Check used before PreEvaluator2
    PreEvaluator2: Check used before PreEvaluator3
    PreEvaluator3: Check used before ConsistencyEvaluatorT

    PreEvaluator1.k() < PreEvaluator2.k()!!!!
    PreEvaluator2.k() < PreEvaluator3.k()!!!!
    PreEvaluator3.k() < K
*/
template<int K,
         class MeasurementT,
         class ConsistencyEvaluatorT,
         class PreEvaluator1,
         class PreEvaluator2,
         class PreEvaluator3>
class GkCMHierarchySolver4
{
  // Data Members
 private:
  GMC::KUniformHypergraph<int> graph_;

  GMC::GeneralizedMaxCliqueSolver<int>& mcs_;

  ConsistencyEvaluatorT& consistency_evaluator_;
  PreEvaluator1& pre_evaluator1_;
  PreEvaluator2& pre_evaluator2_;
  PreEvaluator3& pre_evaluator3_;

  std::vector< MeasurementT> measurements_;
  std::vector<GMC::KUniformHypergraph<int>> hierarchy_graphs_;

  // Function Members
 public:
  GkCMHierarchySolver4(GMC::GeneralizedMaxCliqueSolver<int>& mcs,
            ConsistencyEvaluatorT& consistency_evaluator,
            PreEvaluator1& pre_evaluator1,
            PreEvaluator2& pre_evaluator2,
            PreEvaluator3& pre_evaluator3):
      graph_(K),
      mcs_(mcs),
      consistency_evaluator_(consistency_evaluator),
      pre_evaluator1_(pre_evaluator1),
      pre_evaluator2_(pre_evaluator2),
      pre_evaluator3_(pre_evaluator3),
      measurements_(),
      hierarchy_graphs_{} {
    static_assert(
        std::is_same<MeasurementT,typename ConsistencyEvaluatorT::MeasT>::value,
        "MeasurementT and ConsistencyEvaluatorT:MeasT are not the same");
    assert(pre_evaluator1.k() < pre_evaluator2.k());
    assert(pre_evaluator2.k() < pre_evaluator3.k());
    assert(pre_evaluator3.k() < consistency_evaluator_.k());
    hierarchy_graphs_.push_back(GMC::KUniformHypergraph<int>(pre_evaluator1.k()));
    hierarchy_graphs_.push_back(GMC::KUniformHypergraph<int>(pre_evaluator2.k()));
    hierarchy_graphs_.push_back(GMC::KUniformHypergraph<int>(pre_evaluator3.k()));
  }

  std::vector<std::vector<int>> solve_pcm()
  {
    return this->mcs_.solve(this->graph_);
  }

  std::vector<std::vector<int>> solve()
  {
    return this->mcs_.solve(this->graph_);
  }

  std::vector<std::vector<int>> solve_incremental(std::vector<int> measurements) {
    return this->mcs_.solveIncremental(this->graph_, measurements);
  }

  void clear() {
      measurements_.clear();
      mcs_.clear();
      graph_.clear();
  }

  void add_measurements(std::vector<MeasurementT> measurements)
  {
    std::vector<int> indices;
    indices.reserve(measurements.size());
    for ( auto m : measurements ) {
      indices.push_back(this->measurements_.size());
      this->graph_.add_node(this->measurements_.size());
      for (auto &graph : this->hierarchy_graphs_)
        graph.add_node(this->measurements_.size());
      this->measurements_.push_back(m);
    }

    auto consistent_sets =
        this->pre_evaluator1_.evaluate_consistency_with_all_measurements(
                             indices,
                             this->measurements_);

    this->hierarchy_graphs_[0].add_edges(consistent_sets);
    consistent_sets = this->hierarchy_graphs_[0].get_edges();

    checkConsistency(pre_evaluator2_, this->hierarchy_graphs_[1], indices,
                     this->hierarchy_graphs_[0].get_edges());

    checkConsistency(pre_evaluator3_, this->hierarchy_graphs_[2], indices,
                     this->hierarchy_graphs_[1].get_edges());

    checkConsistency(consistency_evaluator_, graph_, indices,
                     this->hierarchy_graphs_[2].get_edges());
  }

  // Accessors
  int total_num_measurements() { return this->measurements_.size(); }

  const std::vector< MeasurementT >& measurement_map() { return this->measurements_; }

  void print_graph()
  {
    this->graph_.print(std::cout);
  }

  void replace_measurement(MeasurementT meas, int idx) {
    measurements_[idx] = meas;
  }

  std::vector<std::vector<int>> get_consistent_measurements()
  {
    std::vector<std::vector<int>> edges = this->graph_.get_edges();
    std::sort(edges.begin(), edges.end());
    return edges;
  }

 private:
  template<class EvalT>
  void checkConsistency(EvalT &evaluator,
                        GMC::KUniformHypergraph<int> &graph,
                        std::vector<int> indices,
                        std::vector<std::vector<int>> consistent_sets) {
    if (consistent_sets.size() < 1)
      return;
    std::vector<std::vector<int>> sets_to_check =
        find_combinations_that_need_checked(indices, consistent_sets[0].size(),
                                            evaluator.k(), consistent_sets);

    std::vector<typename EvalT::CombT> combinations{};
    std::vector<MeasurementT> meas_comb{};
    for (std::vector<int> set : sets_to_check) {
      meas_comb.clear();
      for (int i : set)
        meas_comb.push_back(this->measurements_[i]);
      combinations.emplace_back(meas_comb);
    }

    std::vector<bool> is_consistent =
        evaluator.evaluate_consistency_of_combinations(combinations);
    for (int i{0}; i != is_consistent.size(); ++i) {
      if (is_consistent[i]) {
        graph.add_edge(sets_to_check[i]);
      }
    }
  }
};

}
#endif //__GkCM_HPP
