#ifndef __GKCM_CF_5DOF_CAMERA_HPP
#define __GKCM_CF_5DOF_CAMERA_HPP

#include <map>
#include <tuple>

#include <Eigen/Dense>

#include "lie/se3.hpp"

#include "gkcm.hpp"

namespace GkCM
{

////////////////////////////////////////////
// 5DOF Camera Factor Measurement
////////////////////////////////////////////
//
// This measurement class stores the data needed for a 5 degree of freedom
// factor type as defined in:
//
//  
//
template<typename NodeIdT>
class Camera5DOFMeasurement
{
 private:
  NodeIdT node_id_a_;
  NodeIdT node_id_b_;

  double azimuth_;
  double elevation_;

  //  std::vector< double > R_ij_; //Row major order matrix 
  Lie::SO3 R_ab_;
  
 public:
  Camera5DOFMeasurement(NodeIdT a,
                        NodeIdT b,
                        double azimuth,
                        double elevation,
                        Lie::SO3 R_ab)
      : node_id_a_(a),
        node_id_b_(b),
        azimuth_(azimuth),
        elevation_(elevation),
        R_ab_(R_ab)
  {}

  NodeIdT id_a() const { return this->node_id_a_; }
  NodeIdT id_b() const { return this->node_id_b_; }

  double azimuth() const { return this->azimuth_; }
  double elevation() const { return this->elevation_; }

  Lie::SO3 R_ab() const { return this->R_ab_; }
};


////////////////////////////////////////////
// 5DOF Camera Factor Consistency Evaluator
////////////////////////////////////////////
template<typename NodeIdT>
class Camera5DOFFactorConsistencyEvaluator : public ConsistencyEvaluator< Camera5DOFMeasurement<NodeIdT> >
{
 private:
  std::map<NodeIdT, Lie::SE3> poses;

  std::tuple<bool, double, double>
  check_rotation_and_deduce_scale_for_measurement_pair(Camera5DOFMeasurement<NodeIdT> meas0,
                                                       Camera5DOFMeasurement<NodeIdT> meas1)
  {
    
    // Get necessary values for measurement pair 0, 1
    Lie::SE3 H_a0 = this->poses[meas0.id_a()];
    Lie::SE3 H_a1 = this->poses[meas1.id_a()];
    Lie::SE3 H_b0 = this->poses[meas0.id_b()];
    Lie::SE3 H_b1 = this->poses[meas1.id_b()];

    Lie::SE3 H_a0a1 = H_a0.inverse() * H_a1;
    Lie::SE3 H_b0b1 = H_b0.inverse() * H_b1;

    // std::cout << "H_a0a1:\n" << H_a0a1() << "\n";
    // std::cout << "H_b0b1:\n" << H_b0b1() << "\n";

    Lie::SO3 R_a0b0 = meas0.R_ab();
    Lie::SO3 R_a1b1 = meas1.R_ab();

    double salpha_0 = sin(meas0.azimuth());
    double calpha_0 = cos(meas0.azimuth());
    double sBeta_0 = sin(meas0.elevation());
    double cBeta_0 = cos(meas0.elevation());
    Eigen::Vector3d A_0;
    A_0 << sBeta_0 * calpha_0, sBeta_0 * salpha_0, cBeta_0;

    double salpha_1 = sin(meas1.azimuth());
    double calpha_1 = cos(meas1.azimuth());
    double sBeta_1 = sin(meas1.elevation());
    double cBeta_1 = cos(meas1.elevation());
    Eigen::Vector3d A_1;
    A_1 << sBeta_1 * calpha_1, sBeta_1 * salpha_1, cBeta_1;

    // Check Rotation is equal to identity
    Lie::SO3 R_Loop =  H_a0a1.R() * R_a1b1 * H_b0b1.R().inverse() * R_a0b0.inverse();
    //    std::cout << "Loop Rotation: \n" << R_Loop() << "\n";

    Lie::SO3 I(Eigen::Matrix3d::Identity());
    bool rotation_loop_valid = R_Loop().isApprox(I(), 1e-4);

    // Find Distance between poses
    //    std::cout << "A0:\n" << A_0 << "\n\nA1:\n" << A_1 << "\n\n";
    
    Eigen::Vector3d X = -1 * A_0;
    Eigen::Vector3d Y = H_a0a1.R() * A_1;
    Eigen::Vector3d Z = H_a0a1.t() - H_a0a1.R() * R_a1b1 * (H_b0b1.R().inverse()) * H_b0b1.t();

    //    std::cout << "X:\n" << X << "\n\nY:\n" << Y << "\n\nZ:\n" << Z << "\n\n";
    
    Eigen::Matrix2d A;
    A <<
        X(0), Y(0),
        X(1), Y(1);
    Eigen::Vector2d b;
    b << -Z(0), -Z(1);

    Eigen::Vector2d d = A.inverse() * b;

    //    std::cout << "Distance Between Poses: \n" << d.transpose() << "\n";

    return std::make_tuple(rotation_loop_valid, d(0), d(1));
  }
  
 public:
  Camera5DOFFactorConsistencyEvaluator()
      :
      ConsistencyEvaluator< Camera5DOFMeasurement<NodeIdT> >(3),
      poses()
  {}
  ~Camera5DOFFactorConsistencyEvaluator() {}

  bool evaluate_consistency(std::vector<int> measurements,
                            const std::vector< Camera5DOFMeasurement<NodeIdT> >& measurement_map)
  {
    assert(measurements.size() == 3);

    auto & meas0 = measurement_map[measurements[0]];
    auto & meas1 = measurement_map[measurements[1]];
    auto & meas2 = measurement_map[measurements[2]];
    
    // Get necessary values for measurement pair 0, 1
    auto result_0_1 = check_rotation_and_deduce_scale_for_measurement_pair(meas0,meas1);
    std::cout << "Results Pair 0, 1: " << std::get<0>(result_0_1) << ", " <<
        std::get<1>(result_0_1) << ", " <<
        std::get<2>(result_0_1) << "\n";
    auto result_0_2 = check_rotation_and_deduce_scale_for_measurement_pair(meas0,meas2);
    std::cout << "Results Pair 0, 2: " << std::get<0>(result_0_2) << ", " <<
        std::get<1>(result_0_2) << ", " <<
        std::get<2>(result_0_2) << "\n";
    auto result_1_2 = check_rotation_and_deduce_scale_for_measurement_pair(meas1,meas2);
    std::cout << "Results Pair 1, 2: " << std::get<0>(result_1_2) << ", " <<
        std::get<1>(result_1_2) << ", " <<
        std::get<2>(result_1_2) << "\n";    
  }

  void add_new_pose(NodeIdT id, Lie::SE3 pose)
  {
    this->poses[id] = pose;
  }
};




}; 
#endif //__GKCM_CF_5DOF_CAMERA_HPP
