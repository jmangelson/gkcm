#include "cugkcm/cugkcm.hpp"

#include "gtest/gtest.h"

TEST(CUGkCM_Thrust_Evaluation_Test, Test1)
{
  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);

  GkCM::Combination< GkCM::Measurement<int>, 2 > comb01({meas0, meas1});
  GkCM::Combination< GkCM::Measurement<int>, 2 > comb12({meas1, meas2});
  GkCM::Combination< GkCM::Measurement<int>, 2 > comb02({meas0, meas2});

  std::vector< GkCM::Combination< GkCM::Measurement<int>, 2 > > combinations = {comb01, comb12, comb02};

  
  auto results = cuGkCM::evaluate_consistency_via_thrust<
    GkCM::Combination< GkCM::Measurement<int>, 2>,
    cuGkCM::CuCombination< cuGkCM::CuMeasurement<int>, 2>,
    cuGkCM::ConsistentIfEqualFunctor< cuGkCM::CuCombination< cuGkCM::CuMeasurement<int>, 2> > >(combinations);

  ASSERT_TRUE(results[0]);
  ASSERT_FALSE(results[1]);
  ASSERT_FALSE(results[2]);
}

TEST(CUGkCM_AreEqualConsistencyEvaluator_Test, IntTest1)
{
  cuGkCM::AreEqualConsistencyEvaluator<int, 2> evaluator;
  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);

  std::vector< GkCM::Measurement<int> > map = {meas0, meas1, meas2};
  std::vector<int> measurements_to_check = {0,1,2};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurements_to_check,
      map,
      true);
  
  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));  
}

TEST(CUGkCM_AreEqualConsistencyEvaluator_Test, DoubleTest1)
{
  cuGkCM::AreEqualConsistencyEvaluator<double, 2> evaluator;
  GkCM::Measurement<double> meas0(3.0);
  GkCM::Measurement<double> meas1(3.0);
  GkCM::Measurement<double> meas2(10.0);

  std::vector< GkCM::Measurement<double> > map = {meas0, meas1, meas2};
  std::vector<int> measurements_to_check = {0,1,2};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurements_to_check,
      map,
      true);
  
  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));  
}

TEST(CUGkCM_AreEqualConsistencyEvaluator_Test, CharTest1)
{
  cuGkCM::AreEqualConsistencyEvaluator<char, 2> evaluator;
  GkCM::Measurement<char> meas0('a');
  GkCM::Measurement<char> meas1('a');
  GkCM::Measurement<char> meas2('c');

  std::vector< GkCM::Measurement<char> > map = {meas0, meas1, meas2};
  std::vector<int> measurements_to_check = {0,1,2};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurements_to_check,
      map,
      true);
  
  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));  
}

TEST(CUGkCM_AreEqualConsistencyEvaluator_Test, FloatTest1)
{
  cuGkCM::AreEqualConsistencyEvaluator<float, 2> evaluator;
  GkCM::Measurement<float> meas0(3.0);
  GkCM::Measurement<float> meas1(3.0);
  GkCM::Measurement<float> meas2(10.0);

  std::vector< GkCM::Measurement<float> > map = {meas0, meas1, meas2};
  std::vector<int> measurements_to_check = {0,1,2};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurements_to_check,
      map,
      true);
  
  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));  
}

TEST(CUGkCM_AreEqualConsistencyEvaluator_Test, BoolTest1)
{
  cuGkCM::AreEqualConsistencyEvaluator<bool, 2> evaluator;
  GkCM::Measurement<bool> meas0(true);
  GkCM::Measurement<bool> meas1(true);
  GkCM::Measurement<bool> meas2(false);

  std::vector< GkCM::Measurement<bool> > map = {meas0, meas1, meas2};
  std::vector<int> measurements_to_check = {0,1,2};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurements_to_check,
      map,
      true);
  
  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));  
}

TEST(CUGkCM_GkCMSolver_Test, Test1)
{
  cuGkCM::AreEqualConsistencyEvaluator<int, 2> evaluator;
  GMC::MaxCliqueSolverHeuristic<int> mcs;
  
  GkCM::Measurement<int> meas0(10);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(0);  
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(15);
  GkCM::Measurement<int> meas5(0);
  GkCM::Measurement<int> meas6(10);
  GkCM::Measurement<int> meas7(10);

  GkCM::GkCMSolver<
    2,
    GkCM::Measurement<int>,
    cuGkCM::AreEqualConsistencyEvaluator<int, 2> >  solver(
        mcs,
        evaluator);

  solver.add_measurements({meas0, meas1, meas2, meas3, meas4, meas5});
  auto solution = solver.solve();

  solver.print_graph();

  GMC::print(std::cout, solution);
  std::cout << "\n";
  
  ASSERT_EQ(3, solution.size());
  ASSERT_TRUE(GMC::contains(solution, {1, 2, 5}));  

  solver.add_measurements({meas6, meas7});
  solution = solver.solve();
  ASSERT_EQ(4, solution.size());
  ASSERT_TRUE(GMC::contains(solution, {0, 3, 6, 7}));

  solver.print_graph();

  GMC::print(std::cout, solution);
  std::cout << "\n";
}
