#include "cugkcm/cugkcm.hpp"

#include "gtest/gtest.h"

TEST(Ints_Are_Equal_Test, Test1)
{
  cuGkCM::AreEqualConsistencyEvaluator<int, 2> evaluator;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);

  std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2};
  std::vector<int> measurements_to_check = {0, 1, 2};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);
  
  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));  
}

TEST(Ints_Are_Equal_Test, Test2)
{
  cuGkCM::AreEqualConsistencyEvaluator<int, 2> evaluator;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);
  GkCM::Measurement<int> meas3(10);  

  std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2, meas3};
  std::vector<int> measurements_to_check = {0, 1, 2, 3};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);
  
  ASSERT_EQ(2, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {2, 3}));    
}

// TEST(DISABLED_Ints_Are_Equal_Test_Triple, DISABLED_Test3)
// {
//   cuGkCM::AreEqualConsistencyEvaluator<int, 3> evaluator;

//   GkCM::Measurement<int> meas0(0);
//   GkCM::Measurement<int> meas1(0);
//   GkCM::Measurement<int> meas2(10);
//   GkCM::Measurement<int> meas3(10);  

//   std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2, meas3};
//   std::vector<int> measurements_to_check = {0, 1, 2, 3};

//   auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);
  
//   ASSERT_EQ(0, consistent_sets.size());
// }

// TEST(DISABLED_Ints_Are_Equal_Test_Triple, DISABLED_Test4)
// {
//   cuGkCM::AreEqualConsistencyEvaluator<int, 3> evaluator;

//   GkCM::Measurement<int> meas0(0);
//   GkCM::Measurement<int> meas1(0);
//   GkCM::Measurement<int> meas2(10);
//   GkCM::Measurement<int> meas3(10);
//   GkCM::Measurement<int> meas4(10);    

//   std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2, meas3, meas4};
//   std::vector<int> measurements_to_check = {0, 1, 2, 3, 4};

//   auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);
  
//   ASSERT_EQ(1, consistent_sets.size());
//   ASSERT_TRUE(GMC::contains(consistent_sets, {2, 3, 4}));  
// }

TEST(Ints_Are_Equal_Test, Test5)
{
  cuGkCM::AreEqualConsistencyEvaluator<int, 2> evaluator;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);    

  std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2, meas3, meas4};
  std::vector<int> measurements_to_check = {0, 1, 2, 3, 4};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);
  
  ASSERT_EQ(4, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));  
  ASSERT_TRUE(GMC::contains(consistent_sets, {2, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {2, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {3, 4}));    
}
