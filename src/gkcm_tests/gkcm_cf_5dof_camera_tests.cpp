//#include "gkcm/cf_5dof_camera.hpp"

#include "gtest/gtest.h"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

// TEST(CF_5DOF_Test, Test1)
// {
//   double PI = 3.14159265359;
//   Lie::SE3 H_a0(0, 0, 0, 0, 0, PI/2);        //Pose 1
//   Lie::SE3 H_a1(1, 0, 0, 0, 0, 3.*PI/4.);    //Pose 2
//   Lie::SE3 H_b0(1, 0, 0, 0, 0, PI/4.);   //Pose 3
//   Lie::SE3 H_b1(1, -0.5, 0, 0, 0, PI/2.);     //Pose 4

//   Lie::SE3 H_B_g(2, 3, 0, 0, 0, PI/8);
//   H_b0 = H_B_g*H_b0;
//   H_b1 = H_B_g*H_b1;

//   GkCM::Camera5DOFMeasurement<int> meas0(1, 3, -PI/2, PI/2, Lie::SO3(0.0, 0.0, -PI/4));
//   GkCM::Camera5DOFMeasurement<int> meas1(2, 4, -5.0*PI/4, PI/2, Lie::SO3(0.0, 0.0, -PI/4));
//   GkCM::Camera5DOFMeasurement<int> meas2(1, 4, -PI/2. - 0.463647609, PI/2, Lie::SO3(0.0, 0.0, 0.0));

//   GkCM::Camera5DOFFactorConsistencyEvaluator<int> consist;
//   consist.add_new_pose(1, H_a0);
//   consist.add_new_pose(2, H_a1);
//   consist.add_new_pose(3, H_b0);
//   consist.add_new_pose(4, H_b1);

//   std::vector<GkCM::Camera5DOFMeasurement<int> > measurement_map = {meas0, meas1, meas2};
//   std::vector<int> measurements_to_check = {0, 1, 2};

//   consist.evaluate_consistency(measurements_to_check, measurement_map);
// }

// TEST(CF_5DOF_Test, Test2)
// {
//   double PI = 3.14159265359;
//   Lie::SE3 H_a0(0, 0, 0, 0, 0, PI/2.);        //Pose 1
//   Lie::SE3 H_a1(1, 0, 0, 0, 0, 3*PI/4.);    //Pose 2
//   Lie::SE3 H_b0(1, 0, 0, 0, 0,  PI/4.);   //Pose 3
//   Lie::SE3 H_b1(2, -1, 0, 0, 0, PI/2);     //Pose 4

//   Lie::SE3 H_B_g(2, 3, 0, 0, 0, PI/8);
//   H_b0 = H_B_g*H_b0;
//   H_b1 = H_B_g*H_b1;

//   GkCM::Camera5DOFMeasurement<int> meas0(1, 3, -PI/2, PI/2, Lie::SO3(0.0, 0.0, -PI/4));
//   GkCM::Camera5DOFMeasurement<int> meas1(2, 4, -PI, PI/2, Lie::SO3(0.0, 0.0, -PI/4));
//   GkCM::Camera5DOFMeasurement<int> meas2(1, 4, -PI/2. - 0.463647609, PI/2, Lie::SO3(0.0, 0.0, 0.0));  

//   GkCM::Camera5DOFFactorConsistencyEvaluator<int> consist;
//   consist.add_new_pose(1, H_a0);
//   consist.add_new_pose(2, H_a1);
//   consist.add_new_pose(3, H_b0);
//   consist.add_new_pose(4, H_b1);

//   std::vector<GkCM::Camera5DOFMeasurement<int> > measurement_map = {meas0, meas1, meas2};
//   std::vector<int> measurements_to_check = {0, 1, 2};

//   consist.evaluate_consistency(measurements_to_check, measurement_map);
// }
