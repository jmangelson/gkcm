#include "gkcm/gkcm.hpp"

#include "gtest/gtest.h"

class ConsistencyEvaluatorDummyTest : public ::testing::Test {
 public:

  template<int K>
  class DummyTrueFunctor
  {
   public:
    bool operator ()(const GkCM::Combination<GkCM::Measurement<int>, K>)
    {
      return true;
    }
  };

  template<int K>
  class AlwaysConsistentEvaluator :
      public GkCM::ConsistencyEvaluator<GkCM::Measurement<int>,
                                        GkCM::Combination<GkCM::Measurement<int>, K>,
                                        DummyTrueFunctor<K>>
                                        
  {
   public:
    AlwaysConsistentEvaluator() {}
    ~AlwaysConsistentEvaluator() {}    
  };

};

TEST_F(ConsistencyEvaluatorDummyTest, Test1)
{
  AlwaysConsistentEvaluator<3> evaluator;

  std::vector<int> measurement_list;
  std::vector<GkCM::Measurement<int>> measurements;
  for(int i=0;i<4;i++)
  {
    measurement_list.push_back(i);
    measurements.push_back(GkCM::Measurement<int>(i));
  }

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurement_list,
      measurements,
      true);

  GMC::print(std::cout, consistent_sets);
  std::cout << "\n";
  
  ASSERT_EQ(4, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 2}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 2, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {1, 2, 3}));
}

TEST_F(ConsistencyEvaluatorDummyTest, Test2)
{
  AlwaysConsistentEvaluator<3> evaluator;

  std::vector<int> measurement_list;
  std::vector<GkCM::Measurement<int>> measurements;
  for(int i=0;i<3;i++)
  {
    measurements.push_back(GkCM::Measurement<int>(i));
  }

  measurement_list.push_back(0);
  measurement_list.push_back(1);
  measurement_list.push_back(2);

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurement_list,
      measurements,
      true);

  GMC::print(std::cout, consistent_sets);
  std::cout << "\n";

  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 2}));
}

TEST_F(ConsistencyEvaluatorDummyTest, Test3)
{
  AlwaysConsistentEvaluator<3> evaluator;

  std::vector<int> measurement_list;
  std::vector<GkCM::Measurement<int>> measurements;
  for(int i=0;i<4;i++)
  {
    measurements.push_back(GkCM::Measurement<int>(i));
  }

  measurement_list.push_back(3);

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurement_list,
      measurements,
      true);

  GMC::print(std::cout, consistent_sets);
  std::cout << "\n";

  ASSERT_EQ(3, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 2, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {1, 2, 3}));
}


TEST_F(ConsistencyEvaluatorDummyTest, Test4)
{
  AlwaysConsistentEvaluator<3> evaluator;

  std::vector<int> measurement_list;
  std::vector<GkCM::Measurement<int>> measurements;
  for(int i=0;i<5;i++)
  {
    measurements.push_back(GkCM::Measurement<int>(i));
  }

  measurement_list.push_back(3);
  measurement_list.push_back(4);

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurement_list,
      measurements,
      true);

  GMC::print(std::cout, consistent_sets);
  std::cout << "\n";

  ASSERT_EQ(9, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 2, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {1, 2, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 2, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {1, 2, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 2, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 3, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {1, 2, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {1, 3, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {2, 3, 4}));    

}

  template<int K>
  class DummyTrueFunctor
  {
   public:
    bool operator ()(const GkCM::Combination<GkCM::Measurement<int>, K>)
    {
      return true;
    }
  };

  template<int K>
  class AlwaysConsistentEvaluator :
      public GkCM::ConsistencyEvaluator<GkCM::Measurement<int>,
                                        GkCM::Combination<GkCM::Measurement<int>, K>,
                                        DummyTrueFunctor<K>>
                                        
  {
   public:
    AlwaysConsistentEvaluator() {}
    ~AlwaysConsistentEvaluator() {}    
  };

class ConsistencyEvaluatorDummyTest2 : public ::testing::Test {
 public:

  class Data
  {
   public:
    int index;
    std::vector<int> consistent_with;
    Data() {}
    Data(int ind, std::vector<int> consistent):
        index(ind), consistent_with(consistent)
    {}
  };
  
  template<int K>
  class MutuallyConsistentFunctor
  {
   public:
    bool operator ()(const GkCM::Combination<GkCM::Measurement<Data>, K> comb)
    {
      int k = K;
      for(int i=0;i<k;i++)
      {
        Data meas_i = comb.get_meas(i).get_value();
        for(int j=0;j<k;j++)
        {
          Data meas_j = comb.get_meas(j).get_value();
          if(i != j &&
             !GMC::contains(meas_j.consistent_with, meas_i.index))
          {
            return false;
          }
        }
      }
      return true;
    }
  };

  template<int K>
  class DummyConsistencyEvaluator :
      public GkCM::ConsistencyEvaluator<GkCM::Measurement<Data>,
                                        GkCM::Combination<GkCM::Measurement<Data>, K>,
                                        MutuallyConsistentFunctor<K>>
                                        
  {
   public:
    DummyConsistencyEvaluator() {}
    ~DummyConsistencyEvaluator() {}    
  };  
};

TEST_F(ConsistencyEvaluatorDummyTest2, Test1)
{
  DummyConsistencyEvaluator<3> evaluator;

  std::vector<int> measurement_list;
  std::vector<GkCM::Measurement< Data >> measurements;

  measurements.push_back(GkCM::Measurement< Data >( Data(0, {1, 2, 3, 4}))); // 0
  measurements.push_back(GkCM::Measurement< Data >( Data(1, {0, 2, 3, 4}))); // 1
  measurements.push_back(GkCM::Measurement< Data >( Data(2, {0, 1, 4}))); // 2
  measurements.push_back(GkCM::Measurement< Data >( Data(3, {0, 1}))); // 3
  measurements.push_back(GkCM::Measurement< Data >( Data(4, {0, 1, 2}))); // 4
  
  
  for(int i=0;i<5;i++)
  {
    measurement_list.push_back(i);
  }

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(
      measurement_list,
      measurements,
      true);

  GMC::print(std::cout, consistent_sets);
  std::cout << "\n";

  ASSERT_EQ(5, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 2}));  
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 2, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {1, 2, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1, 3}));
  
}

