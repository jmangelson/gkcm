#include "gkcm/ce_are_equal.hpp"

#include "gtest/gtest.h"
#include <iostream>

void print(std::vector<int> v) {
  for (int i : v)
    std::cout << i << " ";
  std::cout << std::endl;
}

TEST(Ints_Are_Equal_Test, Test1)
{
  GkCM::AreEqualConsistencyEvaluator<int, 2> evaluator;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);

  std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2};
  std::vector<int> measurements_to_check = {0, 1, 2};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);

  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));
}

TEST(Ints_Are_Equal_Test, Test2)
{
  GkCM::AreEqualConsistencyEvaluator<int, 2> evaluator;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);
  GkCM::Measurement<int> meas3(10);

  std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2, meas3};
  std::vector<int> measurements_to_check = {0, 1, 2, 3};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);

  ASSERT_EQ(2, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {2, 3}));
}

TEST(Ints_Are_Equal_Test, Test3)
{
  GkCM::AreEqualConsistencyEvaluator<int, 3> evaluator;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);
  GkCM::Measurement<int> meas3(10);

  std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2, meas3};
  std::vector<int> measurements_to_check = {0, 1, 2, 3};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);

  ASSERT_EQ(0, consistent_sets.size());
}

TEST(Ints_Are_Equal_Test, Test4)
{
  GkCM::AreEqualConsistencyEvaluator<int, 3> evaluator;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);

  std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2, meas3, meas4};
  std::vector<int> measurements_to_check = {0, 1, 2, 3, 4};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);

  ASSERT_EQ(1, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {2, 3, 4}));
}

TEST(Ints_Are_Equal_Test, Test5)
{
  GkCM::AreEqualConsistencyEvaluator<int, 2> evaluator;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(10);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);

  std::vector<GkCM::Measurement<int> > map = {meas0, meas1, meas2, meas3, meas4};
  std::vector<int> measurements_to_check = {0, 1, 2, 3, 4};

  auto consistent_sets = evaluator.evaluate_consistency_with_all_measurements(measurements_to_check, map, true);

  ASSERT_EQ(4, consistent_sets.size());
  ASSERT_TRUE(GMC::contains(consistent_sets, {0, 1}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {2, 3}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {2, 4}));
  ASSERT_TRUE(GMC::contains(consistent_sets, {3, 4}));
}

TEST(HierarchyOfConsistencyTests, OneHierarchy_BatchTest)
{
  typedef GkCM::AreEqualConsistencyEvaluator<int, 2> Eval2;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 3> Eval3;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(0);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);
  GkCM::Measurement<int> meas5(10);
  GkCM::Measurement<int> meas6(3);
  GkCM::Measurement<int> meas7(4);
  GkCM::Measurement<int> meas8(4);
  GkCM::Measurement<int> meas9(5);
  GkCM::Measurement<int> meas10(6);
  GkCM::Measurement<int> meas11(7);
  GkCM::Measurement<int> meas12(0);

  std::vector<GkCM::Measurement<int>> map{meas0, meas1, meas2, meas3, meas4, meas5, meas6, meas7, meas8, meas9, meas10, meas11, meas12};

  std::vector<std::vector<int>> true_consistent_sets{{0, 1, 2}, {0, 1, 12}, {0, 2, 12}, {1, 2, 12}, {3, 4, 5}};

  Eval3 evaluator;
  Eval2 pre_evaluator;
  GMC::MaxCliqueSolverExact<int> temp(1, 1);
  GMC::GeneralizedMaxCliqueSolver<int> &gmc_solver = temp;
  GkCM::GkCMHierarchySolver2<3, GkCM::Measurement<int>, Eval3, Eval2> solver(gmc_solver, evaluator, pre_evaluator);

  solver.add_measurements(map);
  std::vector<std::vector<int>> consistent_sets = solver.get_consistent_measurements();
  std::sort(true_consistent_sets.begin(), true_consistent_sets.end());
  std::sort(consistent_sets.begin(), consistent_sets.end());

  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[0]));
  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[1]));
  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[2]));
  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[3]));
  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[4]));
}

TEST(HierarchyOfConsistencyTests, OneHierarchy_IncrementalTest)
{
  typedef GkCM::AreEqualConsistencyEvaluator<int, 2> Eval2;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 3> Eval3;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(0);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);
  GkCM::Measurement<int> meas5(10);
  GkCM::Measurement<int> meas6(3);
  GkCM::Measurement<int> meas7(4);
  GkCM::Measurement<int> meas8(4);
  GkCM::Measurement<int> meas9(5);
  GkCM::Measurement<int> meas10(6);
  GkCM::Measurement<int> meas11(7);
  GkCM::Measurement<int> meas12(0);

  std::vector<GkCM::Measurement<int>> map1{meas0, meas1, meas2, meas3, meas4, meas5};
  std::vector<GkCM::Measurement<int>> map2{meas6, meas7, meas8, meas9, meas10, meas11, meas12};

  std::vector<std::vector<int>> true_consistent_sets{{0, 1, 2}, {0, 1, 12}, {0, 2, 12}, {1, 2, 12}, {3, 4, 5}};

  Eval3 evaluator;
  Eval2 pre_evaluator;
  GMC::MaxCliqueSolverExact<int> temp(1, 1);
  GMC::GeneralizedMaxCliqueSolver<int> &gmc_solver = temp;
  GkCM::GkCMHierarchySolver2<3, GkCM::Measurement<int>, Eval3, Eval2> solver(gmc_solver, evaluator, pre_evaluator);
  solver.add_measurements(map1);

  solver.add_measurements(map2);
  std::vector<std::vector<int>> consistent_sets = solver.get_consistent_measurements();
  std::sort(true_consistent_sets.begin(), true_consistent_sets.end());
  std::sort(consistent_sets.begin(), consistent_sets.end());

  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[0]));
  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[1]));
  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[2]));
  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[3]));
  ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[4]));
}

TEST(HierarchyOfConsistencyTests, TwoHierarchies_BatchTest)
{
  typedef GkCM::AreEqualConsistencyEvaluator<int, 2> Eval2;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 3> Eval3;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 4> Eval4;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(0);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);
  GkCM::Measurement<int> meas5(10);
  GkCM::Measurement<int> meas6(3);
  GkCM::Measurement<int> meas7(4);
  GkCM::Measurement<int> meas8(4);
  GkCM::Measurement<int> meas9(5);
  GkCM::Measurement<int> meas10(6);
  GkCM::Measurement<int> meas11(7);
  GkCM::Measurement<int> meas12(0);
  GkCM::Measurement<int> meas13(0);
  GkCM::Measurement<int> meas14(0);
  GkCM::Measurement<int> meas15(10);

  std::vector<GkCM::Measurement<int>> map{meas0, meas1, meas2, meas3, meas4, meas5, meas6, meas7, meas8, meas9, meas10, meas11, meas12, meas13, meas14, meas15};

  std::vector<std::vector<int>> true_consistent_sets{{0, 1, 2, 12}, {0, 1, 2, 13}, {0, 1, 2, 14}, {0, 1, 12, 13}, {0, 1, 12, 14}, {0, 1, 13, 14}, {0, 2, 12, 13}, {0, 2, 12, 14}, {0, 2, 13, 14}, {0, 12, 13, 14}, {1, 2, 12, 13}, {1, 2, 12, 14}, {1, 2, 13, 14}, {1, 12, 13, 14}, {2, 12, 13, 14}, {3, 4, 5, 15}};

  Eval4 evaluator;
  Eval3 pre_evaluator2;
  Eval2 pre_evaluator1;
  GMC::MaxCliqueSolverExact<int> temp(1, 1);
  GMC::GeneralizedMaxCliqueSolver<int> &gmc_solver = temp;
  GkCM::GkCMHierarchySolver3<4, GkCM::Measurement<int>, Eval4, Eval2, Eval3> solver(gmc_solver, evaluator, pre_evaluator1, pre_evaluator2);

  solver.add_measurements(map);
  std::vector<std::vector<int>> consistent_sets = solver.get_consistent_measurements();
  std::sort(true_consistent_sets.begin(), true_consistent_sets.end());
  std::sort(consistent_sets.begin(), consistent_sets.end());

  ASSERT_TRUE(consistent_sets.size() == true_consistent_sets.size());
  for (int i{0}; i != consistent_sets.size(); ++i)
    ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[i]));
}

TEST(HierarchyOfConsistencyTests, TwoHierarchies_IncrementalTest)
{
  typedef GkCM::AreEqualConsistencyEvaluator<int, 2> Eval2;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 3> Eval3;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 4> Eval4;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(0);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);
  GkCM::Measurement<int> meas5(10);
  GkCM::Measurement<int> meas6(3);
  GkCM::Measurement<int> meas7(4);
  GkCM::Measurement<int> meas8(4);
  GkCM::Measurement<int> meas9(5);
  GkCM::Measurement<int> meas10(6);
  GkCM::Measurement<int> meas11(7);
  GkCM::Measurement<int> meas12(0);
  GkCM::Measurement<int> meas13(0);
  GkCM::Measurement<int> meas14(0);
  GkCM::Measurement<int> meas15(10);

  std::vector<GkCM::Measurement<int>> map{meas0, meas1, meas2, meas3, meas4, meas5, meas6, meas7, meas8, meas9, meas10, meas11, meas12};
  std::vector<GkCM::Measurement<int>> map2{meas13, meas14, meas15};

  std::vector<std::vector<int>> true_consistent_sets{{0, 1, 2, 12}, {0, 1, 2, 13}, {0, 1, 2, 14}, {0, 1, 12, 13}, {0, 1, 12, 14}, {0, 1, 13, 14}, {0, 2, 12, 13}, {0, 2, 12, 14}, {0, 2, 13, 14}, {0, 12, 13, 14}, {1, 2, 12, 13}, {1, 2, 12, 14}, {1, 2, 13, 14}, {1, 12, 13, 14}, {2, 12, 13, 14}, {3, 4, 5, 15}};

  Eval4 evaluator;
  Eval3 pre_evaluator2;
  Eval2 pre_evaluator1;
  GMC::MaxCliqueSolverExact<int> temp(1, 1);
  GMC::GeneralizedMaxCliqueSolver<int> &gmc_solver = temp;
  GkCM::GkCMHierarchySolver3<4, GkCM::Measurement<int>, Eval4, Eval2, Eval3> solver(gmc_solver, evaluator, pre_evaluator1, pre_evaluator2);

  solver.add_measurements(map);
  solver.add_measurements(map2);

  std::vector<std::vector<int>> consistent_sets = solver.get_consistent_measurements();
  std::sort(true_consistent_sets.begin(), true_consistent_sets.end());
  std::sort(consistent_sets.begin(), consistent_sets.end());

  ASSERT_TRUE(consistent_sets.size() == true_consistent_sets.size());
  for (int i{0}; i != consistent_sets.size(); ++i)
    ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[i]));
}

TEST(HierarchyOfConsistencyTests, ThreeHierarchies_BatchTest)
{
  typedef GkCM::AreEqualConsistencyEvaluator<int, 2> Eval2;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 3> Eval3;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 4> Eval4;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 5> Eval5;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(0);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);
  GkCM::Measurement<int> meas5(10);
  GkCM::Measurement<int> meas6(3);
  GkCM::Measurement<int> meas7(4);
  GkCM::Measurement<int> meas8(4);
  GkCM::Measurement<int> meas9(5);
  GkCM::Measurement<int> meas10(6);
  GkCM::Measurement<int> meas11(7);
  GkCM::Measurement<int> meas12(0);
  GkCM::Measurement<int> meas13(0);
  GkCM::Measurement<int> meas14(0);
  GkCM::Measurement<int> meas15(10);

  std::vector<GkCM::Measurement<int>> map{meas0, meas1, meas2, meas3, meas4, meas5, meas6, meas7, meas8, meas9, meas10, meas11, meas12, meas13, meas14, meas15};

  std::vector<std::vector<int>> true_consistent_sets{{0, 1, 2, 12, 13}, {0, 1, 2, 12, 14}, {0, 1, 2, 13, 14}, {0, 1, 12, 13, 14}, {0, 2, 12, 13, 14}, {1, 2, 12, 13, 14}};

  Eval5 evaluator;
  Eval4 pre_evaluator3;
  Eval3 pre_evaluator2;
  Eval2 pre_evaluator1;
  GMC::MaxCliqueSolverExact<int> temp(1, 1);
  GMC::GeneralizedMaxCliqueSolver<int> &gmc_solver = temp;
  GkCM::GkCMHierarchySolver4<5, GkCM::Measurement<int>, Eval5, Eval2, Eval3, Eval4> solver(gmc_solver, evaluator, pre_evaluator1, pre_evaluator2, pre_evaluator3);

  solver.add_measurements(map);
  std::vector<std::vector<int>> consistent_sets = solver.get_consistent_measurements();
  std::sort(true_consistent_sets.begin(), true_consistent_sets.end());
  std::sort(consistent_sets.begin(), consistent_sets.end());

  ASSERT_TRUE(consistent_sets.size() == true_consistent_sets.size());
  for (int i{0}; i != consistent_sets.size(); ++i)
    ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[i]));
}

TEST(HierarchyOfConsistencyTests, ThreeHierachies_IncrementalTest)
{
  typedef GkCM::AreEqualConsistencyEvaluator<int, 2> Eval2;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 3> Eval3;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 4> Eval4;
  typedef GkCM::AreEqualConsistencyEvaluator<int, 5> Eval5;

  GkCM::Measurement<int> meas0(0);
  GkCM::Measurement<int> meas1(0);
  GkCM::Measurement<int> meas2(0);
  GkCM::Measurement<int> meas3(10);
  GkCM::Measurement<int> meas4(10);
  GkCM::Measurement<int> meas5(10);
  GkCM::Measurement<int> meas6(3);
  GkCM::Measurement<int> meas7(4);
  GkCM::Measurement<int> meas8(4);
  GkCM::Measurement<int> meas9(5);
  GkCM::Measurement<int> meas10(6);
  GkCM::Measurement<int> meas11(7);
  GkCM::Measurement<int> meas12(0);
  GkCM::Measurement<int> meas13(0);
  GkCM::Measurement<int> meas14(0);
  GkCM::Measurement<int> meas15(10);

  std::vector<GkCM::Measurement<int>> map{meas0, meas1, meas2, meas3, meas4, meas5, meas6, meas7, meas8, meas9, meas10, meas11, meas12};
  std::vector<GkCM::Measurement<int>> map2{meas13, meas14, meas15};

  std::vector<std::vector<int>> true_consistent_sets{{0, 1, 2, 12, 13}, {0, 1, 2, 12, 14}, {0, 1, 2, 13, 14}, {0, 1, 12, 13, 14}, {0, 2, 12, 13, 14}, {1, 2, 12, 13, 14}};

  Eval5 evaluator;
  Eval4 pre_evaluator3;
  Eval3 pre_evaluator2;
  Eval2 pre_evaluator1;
  GMC::MaxCliqueSolverExact<int> temp(1, 1);
  GMC::GeneralizedMaxCliqueSolver<int> &gmc_solver = temp;
  GkCM::GkCMHierarchySolver4<5, GkCM::Measurement<int>, Eval5, Eval2, Eval3, Eval4> solver(gmc_solver, evaluator, pre_evaluator1, pre_evaluator2, pre_evaluator3);

  solver.add_measurements(map);
  solver.add_measurements(map2);
  std::vector<std::vector<int>> consistent_sets = solver.get_consistent_measurements();
  std::sort(true_consistent_sets.begin(), true_consistent_sets.end());
  std::sort(consistent_sets.begin(), consistent_sets.end());

  ASSERT_TRUE(consistent_sets.size() == true_consistent_sets.size());
  for (int i{0}; i != consistent_sets.size(); ++i)
    ASSERT_TRUE(GMC::contains(consistent_sets, true_consistent_sets[i]));
}
