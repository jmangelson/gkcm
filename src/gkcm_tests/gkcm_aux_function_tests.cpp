#include "gkcm/gkcm.hpp"

#include "gtest/gtest.h"

TEST(NChooseKTest, Test1)
{
  ASSERT_EQ(GkCM::n_choose_k(5, 3), 10);
  ASSERT_EQ(GkCM::n_choose_k(3, 1), 3);
  ASSERT_EQ(GkCM::n_choose_k(5, 0), 1);
  ASSERT_EQ(GkCM::n_choose_k(10, 12), 0);
  ASSERT_EQ(GkCM::n_choose_k(12, 10), 66);
}
