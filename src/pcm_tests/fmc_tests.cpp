#include "pcm/pcm.hpp"

#include <iostream>
#include <numeric>

#include <Eigen/Core>

#include "gtest/gtest.h"

TEST(PattExactMaxCliqueTest, Graph2) {
  Eigen::MatrixXi matrix(6,6);
  matrix <<
      1, 1, 1, 1, 0, 1,
      1, 1, 1, 0, 0, 0,
      1, 1, 1, 1, 1, 1,
      1, 0, 1, 1, 0, 1,
      0, 0, 1, 0, 1, 0,
      1, 0, 1, 1, 0, 1;

  PCM::PattabiramanMaxCliqueSolverExact solver;
  auto clique = solver.find_max_clique(matrix);

  int clique_size = 0;
  for (const auto& b : clique)
  {
    if(b)
    {
      clique_size ++;
    }
  }
  
  ASSERT_TRUE(clique_size == 4);

  std::vector<bool> true_map = {true, false, true, true, false, true};
  for(int i=0;i<6;i++)
    ASSERT_TRUE(clique[i] == true_map[i]) << "Returned Clique Doesn't Match True Map";
}

TEST(PattHeuMaxCliqueTest, Graph2) {
  Eigen::MatrixXi matrix(6,6);
  matrix <<
      1, 1, 1, 1, 0, 1,
      1, 1, 1, 0, 0, 0,
      1, 1, 1, 1, 1, 1,
      1, 0, 1, 1, 0, 1,
      0, 0, 1, 0, 1, 0,
      1, 0, 1, 1, 0, 1;

  PCM::PattabiramanMaxCliqueSolverHeuristic solver;
  auto clique = solver.find_max_clique(matrix);

  int clique_size = 0;
  for (const auto& b : clique)
  {
    if(b)
    {
      clique_size ++;
    }
  }
  
  ASSERT_TRUE(clique_size == 4);

  std::vector<bool> true_map = {true, false, true, true, false, true};
  for(int i=0;i<6;i++)
    ASSERT_TRUE(clique[i] == true_map[i]) << "Returned Clique Doesn't Match True Map";
}
