#include "gen_max_clique/gmc.hpp"

#include <chrono>
#include <utility>
#include <map>
#include <fstream>
#include <random>

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::steady_clock;

int main()
{

  std::vector<int> num_thread_steps = {1, 2, 3, 4, 6, 8};
  //std::vector<int> num_thread_steps = {4, 8};
  std::vector<int> num_points_steps = {25, 50, 75, 100, 150, 200, 250, 300};
  //std::vector<int> num_points_steps = {25, 50, 75, 100, 150};
  int num_sample_problems = 100;

  int exact_threshold = 101;
  //  std::vector<int> num_points_steps = {25, 50, 75, 100, 150, 200, 250, 300, 350, 400};
  //  std::vector<int> num_points_steps = {10, 25, 50, 75, 100, 150, 200, 250, 300, 350, 400, 450, 500};


  std::map<int, std::map< int, std::vector< double > > > graph_time;
  std::map<int, std::map< int, std::vector< std::pair < double, bool > > > > heu_results;
  std::map<int, std::map< int, std::vector< std::pair < double, bool > > > > exact_results;
  
  for(auto num_threads : num_thread_steps)
  {
    std::cout << "\n\nNum Threads " << num_threads << "=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*\n";
    for(auto num_points : num_points_steps)
    {
      std::cout << "\n\nNum Points " << num_points << "===========================================================\n";

      graph_time[num_threads][num_points] = {};
      heu_results[num_threads][num_points] = {};
      exact_results[num_threads][num_points] = {};
      
      for(int sample=0; sample < num_sample_problems; sample++)
      {
        // Generate Problemz
        // Enumerate nodes
        std::cout << "Generating Problem " << sample << ", (Num Points: " << num_points << ")---------\n";
        std::vector<int> nodes;
        for(int i=0;i<num_points;i++)
        {
          nodes.push_back(i);
        }

        // Create max clique
        std::vector<int> max_clique;
        std::shuffle(nodes.begin(), nodes.end(), std::default_random_engine(num_points*(sample+1)));
        for(int i=0;i<10; i++)
        {
          max_clique.push_back(nodes[i]);
        }
      
        std::sort(max_clique.begin(), max_clique.end());
        GMC::print(std::cout, max_clique);
        std::cout << "\n";
      
        auto clique_edges = GMC::combinations_of_size(max_clique, 3);

        // Create random edges
        auto all_edges = GMC::combinations_of_size(nodes, 3, true);
        std::shuffle(all_edges.begin(), all_edges.end(), std::default_random_engine(num_points*(sample+1)*2));
  
        double density = 0.1;
        std::vector< std::vector<int> > added_edges;
        for(int i=0; i < density*all_edges.size();i++)
        {
          added_edges.push_back(all_edges[i]);
        }

        // Create Graph
        std::cout << "Creating Graph---------------------------------";
        auto start_graph = std::chrono::high_resolution_clock::now();
        GMC::KUniformHypergraph<int> graph(3);
        graph.add_nodes(nodes);
        graph.add_edges(clique_edges);
        graph.add_edges(added_edges);
        std::chrono::duration<double, std::milli> delta_graph =  (std::chrono::high_resolution_clock::now() - start_graph);

        std::cout << " Time: " << delta_graph.count() << "ms \n";
        graph_time[num_threads][num_points].push_back(delta_graph.count());
        
        // Find clique
        std::cout << "Solving Heuristic------------------------------";
        {
          auto start = std::chrono::high_resolution_clock::now();
          GMC::MaxCliqueSolverHeuristic<int> solver_heu(num_threads);
          auto clique_heu = solver_heu.max_clique_heu(graph, true);
          std::chrono::duration<double, std::milli> delta =  (std::chrono::high_resolution_clock::now() - start);

          bool success = (clique_heu == max_clique);
          std::cout << " Time: " << delta.count() << "ms, success: " << success << " \n";
          heu_results[num_threads][num_points].push_back(std::make_pair(delta.count()+delta_graph.count(), success));
        }

        if(num_points < exact_threshold)
        {
          std::cout << "Solving Exact----------------------------------";
          {
            auto start = std::chrono::high_resolution_clock::now();            
            GMC::MaxCliqueSolverExact<int> solver_exact(num_threads);
            auto clique_exact = solver_exact.max_clique(graph, true);
            std::chrono::duration<double, std::milli> delta =  (std::chrono::high_resolution_clock::now() - start);            

            bool success = (clique_exact == max_clique);
            std::cout << " Time: " << delta.count() << "ms, success: " << success << " \n";
            exact_results[num_threads][num_points].push_back(std::make_pair(delta.count()+delta_graph.count(), success));
          }
        }
      
      }
    }
  }

  std::cout << "\n\n";

  std::map<int, std::map<int, double> > heu_avg_time;
  std::map<int, std::map<int, double> > exact_avg_time;
  std::map<int, std::map<int, double> > heu_percent_correct;
  std::map<int, std::map<int, double> > exact_percent_correct;

  std::ofstream output;
  std::string name = "timing_results.csv";
  output.open(name);
  
  for(auto num_threads : num_thread_steps)
  {
    std::cout << "---------------------------------------------------------\n";
    std::cout << "Num Threads: " << num_threads << " ----------------------------------------\n\n";
    
    for(auto num_points : num_points_steps)
    {
      std::cout << "Num Points: " << num_points << " ---------------------------------\n";


      
      std::vector<double> heu_times;
      heu_times.reserve(heu_results[num_threads][num_points].size());
      int heu_num_correct = 0;
      for(auto p : heu_results[num_threads][num_points])
      {
        heu_times.push_back(p.first);
        if(p.second)
        {
          heu_num_correct++;
        }
      }
      
      heu_avg_time[num_threads][num_points] = std::accumulate(heu_times.begin(), heu_times.end(), 0.0)/heu_times.size();
      heu_percent_correct[num_threads][num_points] = heu_num_correct/heu_times.size()*100.0;

      std::cout << "Heuristic Avg Time: " << heu_avg_time[num_threads][num_points] << "\n";
      std::cout << "Heuristic Percent Correct: " << heu_percent_correct[num_threads][num_points] << "\n";

      output << "Heu, " << num_threads << ", " << num_points << ", " << heu_avg_time[num_threads][num_points] <<
          ", " << heu_percent_correct[num_threads][num_points] << "\n";

      if(exact_results[num_threads][num_points].size() > 0)
      {
        std::vector<double> exact_times;
        exact_times.reserve(exact_results[num_threads][num_points].size());
        int exact_num_correct = 0;
        for(auto p : exact_results[num_threads][num_points])
        {
          exact_times.push_back(p.first);
          if(p.second)
          {
            exact_num_correct++;
          }
        }
      
        exact_avg_time[num_threads][num_points] = std::accumulate(exact_times.begin(), exact_times.end(), 0.0)/exact_times.size();
        exact_percent_correct[num_threads][num_points] = exact_num_correct/exact_times.size()*100.0;

        std::cout << "Exact Avg Time: " << exact_avg_time[num_threads][num_points] << "\n";
        std::cout << "Exact Percent Correct: " << exact_percent_correct[num_threads][num_points] << "\n";

        output << "Exact, " << num_threads << ", " << num_points << ", " <<
            exact_avg_time[num_threads][num_points] << ", " <<
            exact_percent_correct[num_threads][num_points] << "\n";
      }
      
      std::cout << "\n";
    }
  }

  output.close();
}
