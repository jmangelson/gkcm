#!/usr/bin/env python

from matplotlib.figure import Figure
import matplotlib.pyplot as plt

import numpy as np
import csv


def read_timing_results(file_name):
    with open(file_name, 'rb') as csvfile:
        data_reader = csv.reader(csvfile)
        data_heu_avgtime = {}
        data_heu_percent_correct = {}
        data_exact_avgtime = {}
        data_exact_percent_correct = {}        
        for row in data_reader:
            if(row[0] == 'Heu'):
                data_heu_avgtime[(float(row[1]), float(row[2]))] = float(row[3])
                data_heu_percent_correct[(float(row[1]), float(row[2]))] = float(row[4])
            if(row[0] == 'Exact'):
                data_exact_avgtime[(float(row[1]), float(row[2]))] = float(row[3])
                data_exact_percent_correct[(float(row[1]), float(row[2]))] = float(row[4])            
        return (data_heu_avgtime, data_heu_percent_correct, data_exact_avgtime, data_exact_percent_correct)

def plot_avg_time_comparison(data,
                             ax,
                             threads=[1, 2, 3, 4, 6, 8],
                             num_points_steps=[25, 50, 75, 100, 150, 200, 250, 300],
                             classes=['Heu', 'Exact']):
    colors = ['b', 'g', 'r', 'k', 'm', 'y']
    
            
    if 'Exact' in classes:
        data_exact_avgtime = data[2]
        index = 0
        for num_threads in threads:
            points = []
            avg_time = []
            for num_points in num_points_steps:
                if (num_threads, num_points) in data_exact_avgtime:
                    avg_time.append(data_exact_avgtime[(num_threads, num_points)]/1000.0)
                    points.append(num_points)

            label = 'Exact-' + str(num_threads) + ' threads'
            #            ax.plot(points, avg_time, label=label)
            ax.semilogy(points, avg_time, label=label, color=colors[index])
            index = index+1

    if 'Heu' in classes:
        data_heu_avgtime = data[0]
        index = 0
        for num_threads in threads:
            points = []
            avg_time = []
            for num_points in num_points_steps:
                if (num_threads, num_points) in data_heu_avgtime:
                    avg_time.append(data_heu_avgtime[(num_threads, num_points)]/1000.0)
                    points.append(num_points)

            label = 'Heu-' + str(num_threads) + ' threads'
            #            ax.plot(points, avg_time, label=label)
            ax.semilogy(points, avg_time, label=label, linestyle="--", color=colors[index])
            index = index + 1

if __name__ == '__main__':
    plt.rcParams.update({'font.size': 18, 'legend.fontsize':10})
    
    data = read_timing_results('../../timing_results.csv')

    fig = plt.figure()
    ax = plt.gca()
    
    plot_avg_time_comparison(data, ax)
    ax.legend(bbox_to_anchor=(0., -0.05, 1.0, -0.15), loc=2,
           ncol=4, mode="expand", borderaxespad=0.)
    fig.subplots_adjust(bottom=0.25)
    ax.set_xlabel('Number of Nodes in Graph')
    ax.set_ylabel('Run Time (sec)')
    
    plt.title('Timing Comparison')
    
    plt.show()
