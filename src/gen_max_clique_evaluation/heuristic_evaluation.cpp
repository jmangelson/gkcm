#include "gen_max_clique/gmc.hpp"

#include <chrono>
#include <utility>
#include <map>
#include <fstream>
#include <random>

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::steady_clock;

bool is_true(bool b) {return b;}

int main()
{


  std::vector<double> graph_density_steps = {0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4};

  //  std::vector<int> max_clique_size_steps = {5, 8, 11, 14, 17, 20, 23, 26, 29};
  std::vector<int> max_clique_size_steps = {5, 8, 11, 14};//, 17, 20, 23, 26, 29};



  int num_nodes = 100;
  //  int num_sample_problems = 100;
  int num_sample_problems = 100;

  std::map<int, std::map<double, int> > num_invalid_graphs;
  std::map<double, std::map<int, std::vector<bool> > > success;
  std::map<double, std::map<int, std::vector<double> > > time;

  for(auto max_clique_size : max_clique_size_steps)
  {
    std::cout << "\n\nMax Clique Size: " << max_clique_size << "=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*\n";

    //    num_invalid_graphs[max_clique_size] = 0;
      
    for(auto graph_density : graph_density_steps)
    {
      std::cout << "\n\nGraph Density " << graph_density << "----------------------------------------------------------------------\n";

      success[graph_density][max_clique_size] = {};
      num_invalid_graphs[max_clique_size][graph_density] = 0;
      
      for(int sample=0; sample < num_sample_problems; sample++)
      {
        // Generate Problemz
        // Enumerate nodes
        std::cout << "Generating Problem " << sample << "-----------------------\n";
        std::vector<int> nodes;
        for(int i=0;i<num_nodes;i++)
        {
          nodes.push_back(i);
        }

        // Create max clique
        std::vector<int> max_clique;
        std::shuffle(nodes.begin(), nodes.end(), std::default_random_engine(graph_density*100*max_clique_size*(sample+1)));
        for(int i=0;i<max_clique_size; i++)
        {
          max_clique.push_back(nodes[i]);
        }
      
        std::sort(max_clique.begin(), max_clique.end());
        GMC::print(std::cout, max_clique);
        std::cout << "\n";
      
        auto clique_edges = GMC::combinations_of_size(max_clique, 3);

        // Create random edges
        auto all_edges = GMC::combinations_of_size(nodes, 3, true);
        std::shuffle(all_edges.begin(), all_edges.end(), std::default_random_engine(graph_density*100*max_clique_size*(sample+1)*2));
  
        double density = graph_density;
        std::vector< std::vector<int> > added_edges;
        for(int i=0; i < density*all_edges.size();i++)
        {
          added_edges.push_back(all_edges[i]);
        }

        // Create Graph
        std::cout << "Creating Graph---------------------------------";
        auto start_graph = std::chrono::high_resolution_clock::now();
        GMC::KUniformHypergraph<int> graph(3);
        graph.add_nodes(nodes);
        graph.add_edges(clique_edges);
        graph.add_edges(added_edges);
        std::chrono::duration<double, std::milli> delta_graph =  (std::chrono::high_resolution_clock::now() - start_graph);

        std::cout << " Time: " << delta_graph.count() << "ms \n";
        
        // Find clique
        std::cout << "Solving Heuristic------------------------------";
        {
          auto start = std::chrono::high_resolution_clock::now();
          GMC::MaxCliqueSolverHeuristic<int> solver_heu(8);
          auto clique_heu = solver_heu.max_clique_heu(graph, true);
          std::chrono::duration<double, std::milli> delta =  (std::chrono::high_resolution_clock::now() - start);

          bool success_loc = (clique_heu.size() == max_clique.size());
          if(clique_heu.size() <= max_clique.size())
          {
            success[graph_density][max_clique_size].push_back(success_loc);
            time[graph_density][max_clique_size].push_back(delta.count()+delta_graph.count());
            std::cout << " Time: " << delta.count()+delta_graph.count() << "ms, success: " << success_loc << " \n";
            GMC::print(std::cout, clique_heu);
            std::cout << "\n";
          }
          else
          {
            std::cout << "Invalid Graph!!! Heuristic found larger max clique!!!!!!!!!!\n\n";
            num_invalid_graphs[max_clique_size][graph_density]++;
          }
        }

        std::cout << "\n";
      }

    }

  }
  std::cout << "\n\n";
  for(auto p : num_invalid_graphs)
  {
    int size = p.first;
    for(auto q : p.second)
    {
      std::cout << "Times Heu Found Larger Clique for MaxCliqueSize " << size << ", " << q.first << ": " << q.second << "\n";
    }
  }

  std::cout << "\n";
  
  std::map<double, std::map<int, double > > percent_correct;
  std::map<double, std::map<int, double > > avg_time;

  std::ofstream output;
  std::string name = "heuristic_evaluation_results.csv";
  output.open(name);

  for(auto graph_density : graph_density_steps)
  {
    for(auto max_clique_size : max_clique_size_steps)
    {
    
      percent_correct[graph_density][max_clique_size] =
          std::count_if(success[graph_density][max_clique_size].begin(),
                        success[graph_density][max_clique_size].end(),
                        is_true)/(1.0*success[graph_density][max_clique_size].size())*100.0;
      avg_time[graph_density][max_clique_size] =
          std::accumulate(time[graph_density][max_clique_size].begin(),
                          time[graph_density][max_clique_size].end(),
                          0.0)/time[graph_density][max_clique_size].size();
          
      std::cout << "Graph Density: " << graph_density << ", Max Clique Size: " << max_clique_size 
                << ", Success %: " << percent_correct[graph_density][max_clique_size]
                << ", Avg Time: " << avg_time[graph_density][max_clique_size] << " ms \n";

      output << graph_density << ", " << max_clique_size << ", "
             << percent_correct[graph_density][max_clique_size] << ", "
             << avg_time[graph_density][max_clique_size] << "\n";

    }
  }

  output.close();
}
