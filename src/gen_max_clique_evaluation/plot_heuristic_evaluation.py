#!/usr/bin/env python

from matplotlib.figure import Figure
import matplotlib.pyplot as plt

import numpy as np
import csv


def read_heuristic_evaluation_results(file_name):
    with open(file_name, 'rb') as csvfile:
        data_reader = csv.reader(csvfile)
        data_avgtime = {}
        data_percent_correct = {}
        for row in data_reader:
                data_avgtime[(float(row[0]), float(row[1]))] = float(row[3])
                data_percent_correct[(float(row[0]), float(row[1]))] = float(row[2])
        return (data_avgtime, data_percent_correct)

def plot_heuristic_evaluation(data,
                              ax,
                              max_clique_size_steps=[5, 8, 11, 14, 17, 20, 23, 26, 29],
                              graph_density_steps=[0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]):
    markers = ['^', '8', 'v', 'p', 'h', 's', '*', 'x', 'D']
    index = 0
    
    data_percent_correct = data[1]
    for max_clique_size in max_clique_size_steps:
        graph_densities =[]
        percent_correct = []
        for graph_density in graph_density_steps:
            if (graph_density, max_clique_size) in data_percent_correct:
                percent_correct.append(data_percent_correct[(graph_density, max_clique_size)])
                graph_densities.append(graph_density)

        label = 'MaxClique-' + str(max_clique_size)
        #            ax.plot(points, avg_time, label=label)
        ax.plot(graph_densities, percent_correct, label=label, marker=markers[index])
        index = index + 1

if __name__ == '__main__':
    plt.rcParams.update({'font.size': 18, 'legend.fontsize':14})
    
    data = read_heuristic_evaluation_results('../../heuristic_evaluation_results.csv')

    fig = plt.figure()
    ax = plt.gca()
    
    plot_heuristic_evaluation(data, ax)
    ax.legend(bbox_to_anchor=(0., -0.05, 1.0, -0.15), loc=2,
           ncol=3, mode="expand", borderaxespad=0.)
    fig.subplots_adjust(bottom=0.3)
    ax.set_xlabel('Graph Density')
    ax.set_ylabel('Percent Correct')
    ax.set_ylim([-1, 101])
    ax.set_xlim([0.04, 0.41])
    
    plt.title('Heuristic Evaluation')
    
    plt.show()
