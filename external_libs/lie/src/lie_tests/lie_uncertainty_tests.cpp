#include "lie/se3.hpp"
#include "lie/se2.hpp"

#include "mr/stat.hpp"

#include "gtest/gtest.h"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

Eigen::MatrixXd expand_2D_covariance(const Eigen::MatrixXd& cov_6x6)
{
  Eigen::MatrixXd cov_12x12 = Eigen::MatrixXd::Zero(12,12);
  int a, b, c, d;
  a = b = 0; c = d = 0;
  cov_12x12.block(0+a,0+b,2,2) = cov_6x6.block(0+c,0+d,2,2);
  cov_12x12.block(0+a,5+b,2,1) = cov_6x6.block(0+c,2+d,2,1);
  cov_12x12.block(5+a,0+b,1,2) = cov_6x6.block(2+c,0+d,1,2);
  cov_12x12(5+a,5+b) = cov_6x6(2+c,2+d);

  a = 6; b = 0; c = 3; d = 0;
  cov_12x12.block(0+a,0+b,2,2) = cov_6x6.block(0+c,0+d,2,2);
  cov_12x12.block(0+a,5+b,2,1) = cov_6x6.block(0+c,2+d,2,1);
  cov_12x12.block(5+a,0+b,1,2) = cov_6x6.block(2+c,0+d,1,2);
  cov_12x12(5+a,5+b) = cov_6x6(2+c,2+d);

  a = 0; b = 6; c = 0; d = 3;  
  cov_12x12.block(0+a,0+b,2,2) = cov_6x6.block(0+c,0+d,2,2);
  cov_12x12.block(0+a,5+b,2,1) = cov_6x6.block(0+c,2+d,2,1);
  cov_12x12.block(5+a,0+b,1,2) = cov_6x6.block(2+c,0+d,1,2);
  cov_12x12(5+a,5+b) = cov_6x6(2+c,2+d);

  a = b = 6; c = d = 3;
  cov_12x12.block(0+a,0+b,2,2) = cov_6x6.block(0+c,0+d,2,2);
  cov_12x12.block(0+a,5+b,2,1) = cov_6x6.block(0+c,2+d,2,1);
  cov_12x12.block(5+a,0+b,1,2) = cov_6x6.block(2+c,0+d,1,2);
  cov_12x12(5+a,5+b) = cov_6x6(2+c,2+d);

  return cov_12x12;
}

Eigen::MatrixXd expand_2D_covariance_3x3(const Eigen::MatrixXd& cov_3x3)
{
  Eigen::MatrixXd cov_6x6 = Eigen::MatrixXd::Zero(6,6);
  int a, b, c, d;
  a = b = 0; c = d = 0;
  cov_6x6.block(0+a,0+b,2,2) = cov_3x3.block(0+c,0+d,2,2);
  cov_6x6.block(0+a,5+b,2,1) = cov_3x3.block(0+c,2+d,2,1);
  cov_6x6.block(5+a,0+b,1,2) = cov_3x3.block(2+c,0+d,1,2);
  cov_6x6(5+a,5+b) = cov_3x3(2+c,2+d);

  return cov_6x6;
}

Lie::SE3 SE2_mat_to_SE3(const Eigen::MatrixXd& H)
{
  double x, y, z, r, p, h;
  z = 0;
  r = 0;
  p = 0;

  x = H(0,2);
  y = H(1,2);
  h = atan2(H(1,0), H(0,0));

  return Lie::SE3(x, y, z, r, p, h);
}

TEST(SE2_SE3_Equal_Test, BetweenTest1)
{
  double PI = 3.14159265359;

  // Check 2d
  Eigen::MatrixXd cov_g1g2_2d = MR::STAT::rand_cov(6);
  Lie::SE2 H_g1(1, 2, PI/3);
  Lie::SE2 H_g2(2, 3, PI/3);  

  auto joint_2d = Lie::make_uncertain_state(
      std::make_tuple(H_g1, H_g2),
      cov_g1g2_2d);

  auto T_g1 = std::get<0>(joint_2d);
  auto T_g2 = std::get<1>(joint_2d);

  auto T_12 = Lie::between(T_g1, T_g2);

  // Check 3d
  Eigen::MatrixXd cov_g1g2_3d = expand_2D_covariance(cov_g1g2_2d);
  Lie::SE3 H_g1_3d = SE2_mat_to_SE3(H_g1());
  Lie::SE3 H_g2_3d = SE2_mat_to_SE3(H_g2());

  auto joint_3d = Lie::make_uncertain_state(
      std::make_tuple(H_g1_3d, H_g2_3d),
      cov_g1g2_3d);  
  auto T_g1_3d = std::get<0>(joint_3d);
  auto T_g2_3d = std::get<1>(joint_3d);

  auto T_12_3d = Lie::between(T_g1_3d, T_g2_3d);

  // Check That They Match
  Eigen::MatrixXd T_12_2d_converted(4,4);
  T_12_2d_converted
      << T_12.mu().R()(), Eigen::Vector2d::Zero(), T_12.mu().t(),
      Eigen::RowVector2d::Zero(), 1, 0,
      0, 0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(T_12_2d_converted, T_12_3d.mu()());

  Eigen::MatrixXd Marginal_2d_converted(6,6);
  Marginal_2d_converted
      << T_12.marginal_cov().topLeftCorner(2,2), Eigen::MatrixXd::Zero(2,3), T_12.marginal_cov().topRightCorner(2,1),
      Eigen::MatrixXd::Zero(3,2),       Eigen::MatrixXd::Zero(3,3),       Eigen::MatrixXd::Zero(3,1),
      T_12.marginal_cov().bottomLeftCorner(1,2), Eigen::MatrixXd::Zero(1,3), T_12.marginal_cov().bottomRightCorner(1,1);

  ASSERT_MATRICES_APPROX_EQ(Marginal_2d_converted, T_12_3d.marginal_cov());  
}

TEST(SE2_SE3_Equal_Test, ComposeTest1)
{
  double PI = 3.14159265359;

  // Check 2d
  Eigen::MatrixXd cov_g112_2d = MR::STAT::rand_cov(6);
  Lie::SE2 H_g1(1, 2, PI/3);
  Lie::SE2 H_12(2, 3, PI/3);  

  auto joint_2d = Lie::make_uncertain_state(
      std::make_tuple(H_g1, H_12),
      cov_g112_2d);

  auto T_g1 = std::get<0>(joint_2d);
  auto T_12 = std::get<1>(joint_2d);

  auto T_g2 = Lie::compose(T_g1, T_12);

  // Check 3d
  Eigen::MatrixXd cov_g112_3d = expand_2D_covariance(cov_g112_2d);
  Lie::SE3 H_g1_3d = SE2_mat_to_SE3(H_g1());
  Lie::SE3 H_12_3d = SE2_mat_to_SE3(H_12());

  auto joint_3d = Lie::make_uncertain_state(
      std::make_tuple(H_g1_3d, H_12_3d),
      cov_g112_3d);  
  auto T_g1_3d = std::get<0>(joint_3d);
  auto T_12_3d = std::get<1>(joint_3d);

  auto T_g2_3d = Lie::compose(T_g1_3d, T_12_3d);

  // Check That They Match
  Eigen::MatrixXd T_g2_2d_converted(4,4);
  T_g2_2d_converted
      << T_g2.mu().R()(), Eigen::Vector2d::Zero(), T_g2.mu().t(),
      Eigen::RowVector2d::Zero(), 1, 0,
      0, 0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(T_g2_2d_converted, T_g2_3d.mu()());

  Eigen::MatrixXd Marginal_2d_converted(6,6);
  Marginal_2d_converted << expand_2D_covariance_3x3(T_g2.marginal_cov());

  ASSERT_MATRICES_APPROX_EQ(Marginal_2d_converted, T_g2_3d.marginal_cov());  
}

TEST(SE2_SE3_Equal_Test, InverseTest1)
{
  double PI = 3.14159265359;

  // Check 2d
  Eigen::MatrixXd cov_2d = MR::STAT::rand_cov(3);
  Lie::SE2 H_g1(1, 2, PI/3);

  auto T_g1 = Lie::make_uncertain_state(
      H_g1, cov_2d);

  auto T_1g = T_g1.inverse();

  // Check 3d
  Eigen::MatrixXd cov_3d = expand_2D_covariance_3x3(cov_2d);
  Lie::SE3 H_g1_3d = SE2_mat_to_SE3(H_g1());

  auto T_g1_3d = Lie::make_uncertain_state(
      H_g1_3d, cov_3d);  

  auto T_1g_3d = T_g1_3d.inverse();

  // Check That They Match
  ASSERT_MATRICES_APPROX_EQ(SE2_mat_to_SE3(T_1g.mu()())(), T_1g_3d.mu()());
  ASSERT_MATRICES_APPROX_EQ(expand_2D_covariance_3x3(T_1g.marginal_cov()), T_1g_3d.marginal_cov());  
}
