#include "lie/so3.hpp"

#include "gtest/gtest.h"

#define ASSERT_MATRICES_APPROX_EQ(M_actual, M_expected) \
  ASSERT_TRUE(M_actual.isApprox(M_expected, 1e-6)) << "  Actual:\n" << M_actual << "\nExpected:\n" << M_expected

TEST(SO3ConstructionTest, CopyTest)
{
  double PI = 3.14159265359;
  Lie::SO3 R(PI/2, 0.0, 0.0);

  Eigen::Matrix3d R_true;
  R_true <<
      1, 0, 0,
      0, 0, -1,
      0, 1, 0;

  Lie::SO3 R2 = R;
  Lie::SO3 R3(R);
  
  ASSERT_MATRICES_APPROX_EQ(R2(), R_true);
  ASSERT_MATRICES_APPROX_EQ(R3(), R_true);
}

TEST(SO3ConstructionTest, MatrixConstuctorTest)
{
  Eigen::Matrix3d R_true;
  R_true <<
      1, 0, 0,
      0, 0, -1,
      0, 1, 0;

  Lie::SO3 R(R_true);
  
  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}


TEST(SO3ConstructionTest, RollTest1)
{
  double PI = 3.14159265359;
  Lie::SO3 R(PI/2, 0.0, 0.0);

  Eigen::Matrix3d R_true;
  R_true <<
      1, 0, 0,
      0, 0, -1,
      0, 1, 0;
  
  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(SO3ConstructionTest, PitchTest1)
{
  double PI = 3.14159265359;
  Lie::SO3 R(0.0, PI/2, 0.0);

  Eigen::Matrix3d R_true;
  R_true <<
      0, 0, 1,
      0, 1, 0,
      -1, 0, 0;      
  
  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(SO3ConstructionTest, YawTest1)
{
  double PI = 3.14159265359;
  Lie::SO3 R(0.0, 0.0, PI/2);

  Eigen::Matrix3d R_true;
  R_true <<
      0, -1, 0,
      1, 0, 0,
      0, 0, 1;

  
  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(SO3MultTest, Test1)
{
  double PI = 3.14159265359;
  Lie::SO3 R1(PI/2, 0, 0);
  Lie::SO3 R2(PI/2, 0, 0);
  Lie::SO3 R3(PI/2, 0, 0);
  Lie::SO3 R4(PI/2, 0, 0);

  Eigen::Matrix3d R_true;
  R_true <<
      1, 0, 0,
      0, 1, 0,
      0, 0, 1;


  auto R = R1*R2*R3*R4;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(SO3MultTest, Test2)
{
  double PI = 3.14159265359;
  Lie::SO3 R1(0.0, 0, PI/2);
  Eigen::Vector3d vec;
  vec << 1, 0, 0;

  Eigen::Vector3d V_true;
  V_true << 0, 1, 0;


  auto V = R1*vec;

  ASSERT_MATRICES_APPROX_EQ(V, V_true);
}

TEST(SO3ComposeTest, Test1)
{
  double PI = 3.14159265359;
  Lie::SO3 R1(PI/2, 0, 0);
  Lie::SO3 R2(PI/2, 0, 0);
  Lie::SO3 R3(PI/2, 0, 0);
  Lie::SO3 R4(PI/2, 0, 0);

  Eigen::Matrix3d R_true;
  R_true <<
      1, 0, 0,
      0, 1, 0,
      0, 0, 1;


  auto R_Compose1 = R1.compose(R2).compose(R3).compose(R4);
  auto R_Compose2 = compose(compose(R1, R2), compose(R3, R4));

  ASSERT_MATRICES_APPROX_EQ(R_Compose1(), R_true);
  ASSERT_MATRICES_APPROX_EQ(R_Compose2(), R_true);
}

TEST(SO3BetweenTest, Test1)
{
  double PI = 3.14159265359;
  Lie::SO3 R1(0, 0, 0);
  Lie::SO3 R2(PI/2, 0, 0);
  Lie::SO3 R3(PI, 0, 0);

  Lie::SO3 R4(-PI, 0, 0);


  auto R_Between1a = R1.between(R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between1a(), R3());
  auto R_Between1b = between(R1, R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between1b(), R3());
  
  auto R_Between2a = R1.between(R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between2a(), R3());
  auto R_Between2b = between(R1, R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between2b(), R3());  

  auto R_Between3a = R2.between(R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between3a(), R2());
  auto R_Between3b = between(R2, R3);
  ASSERT_MATRICES_APPROX_EQ(R_Between3b(), R2());  

  auto R_Between4a = R3.between(R1);
  ASSERT_MATRICES_APPROX_EQ(R_Between4a(), R4());
  auto R_Between4b = between(R3, R1);
  ASSERT_MATRICES_APPROX_EQ(R_Between4b(), R4());  
}



TEST(SO3InvTest, Test1)
{
  Lie::SO3 R1(4, 5, 6);

  Eigen::Matrix3d R_true;
  R_true <<
      1, 0, 0,
      0, 1, 0,
      0, 0, 1;

  auto R = R1.inverse()*R1;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(ExpTest, Test1)
{
  double PI = 3.14159265359;
  Eigen::Vector3d w;
  w << PI/2., 0, 0;

  Lie::SO3::TangentVector phi(w);

  Lie::SO3 R = Lie::SO3::Exp(phi);

  Eigen::Matrix3d R_true;
  R_true <<
      1, 0, 0,
      0, 0, -1,
      0, 1, 0;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}


TEST(ExpTest, Test2)
{
  double PI = 3.14159265359;
  Eigen::Vector3d w;
  w << 0, PI/2., 0;

  Lie::SO3::TangentVector phi(w);

  Lie::SO3 R = Lie::SO3::Exp(phi);

  Eigen::Matrix3d R_true;
  R_true <<
      0, 0, 1,
      0, 1, 0,
     -1, 0, 0;        

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}


TEST(ExpTest, Test3)
{
  double PI = 3.14159265359;
  Eigen::Vector3d w;
  w << 0, 0, PI/2.;

  Lie::SO3::TangentVector phi(w);

  Lie::SO3 R = Lie::SO3::Exp(phi);

  Eigen::Matrix3d R_true;
  R_true <<
      0, -1, 0,
      1, 0, 0,
      0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(ExpTest, ZeroTest)
{
  Eigen::Vector3d w;
  w << 0, 0, 0.;

  Lie::SO3::TangentVector phi(w);

  Lie::SO3 R = Lie::SO3::Exp(phi);

  Eigen::Matrix3d R_true;
  R_true <<
      1, 0, 0,
      0, 1, 0,
      0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(R(), R_true);
}

TEST(LogTest, Test1)
{
  double PI = 3.14159265359;
  Eigen::Vector3d w_true;
  w_true << PI/2., 0, 0;

  Eigen::Matrix3d R;
  R <<
      1, 0, 0,
      0, 0, -1,
      0, 1, 0;
  Lie::SO3 Rot(R);
  
  Lie::SO3::TangentVector w = Lie::SO3::Log(Rot);

  ASSERT_MATRICES_APPROX_EQ(w, w_true);

}

TEST(LogTest, Test2)
{
  double PI = 3.14159265359;
  Eigen::Vector3d w_true;
  w_true << 0, PI/2., 0;

  Eigen::Matrix3d R;
  R <<
      0, 0, 1,
      0, 1, 0,
      -1, 0, 0;          
  Lie::SO3 Rot(R);
  
  Lie::SO3::TangentVector w = Lie::SO3::Log(Rot);

  ASSERT_MATRICES_APPROX_EQ(w, w_true);
}

TEST(LogTest, Test3)
{
  double PI = 3.14159265359;
  Eigen::Vector3d w_true;
  w_true << 0, 0, PI/2.;

  Eigen::Matrix3d R;
  R <<
      0, -1, 0,
      1, 0, 0,
      0, 0, 1;  
  Lie::SO3 Rot(R);
  
  Lie::SO3::TangentVector w = Lie::SO3::Log(Rot);

  ASSERT_MATRICES_APPROX_EQ(w, w_true);
}

TEST(LogTest, ZeroTest)
{
  double PI = 3.14159265359;
  Eigen::Vector3d w_true;
  w_true << 0, 0, 0;

  Eigen::Matrix3d R;
  R <<
      1, 0, 0,
      0, 1, 0,
      0, 0, 1;

  Lie::SO3 Rot(R);
  
  Lie::SO3::TangentVector w = Lie::SO3::Log(Rot);

  ASSERT_MATRICES_APPROX_EQ(w, w_true);
}



TEST(LogExpTest, Test1)
{
  double PI = 3.14159265359;
  Eigen::Vector3d w_true;
  w_true << PI/2., PI/3., PI/4.;

  Lie::SO3::TangentVector w(w_true);
  Lie::SO3::TangentVector w_prime = Lie::SO3::Log(Lie::SO3::Exp(w));

  ASSERT_MATRICES_APPROX_EQ(w_prime, w_true);

}

TEST(ExpLogTest, Test1)
{
  double PI = 3.14159265359;

  Eigen::Matrix3d R_true;
  R_true <<
      0, -1, 0,
      1, 0, 0,
      0, 0, 1;
  
  Lie::SO3 Rot(R_true);
  Lie::SO3 Rot_prime = Lie::SO3::Exp(Lie::SO3::Log(Rot));

  ASSERT_MATRICES_APPROX_EQ(Rot_prime(), R_true);

}

TEST(LeftJacobianTest, Test1)
{
  Eigen::Vector3d vec;
  vec << 0.5269, 0.4168, 0.6569;
  Lie::SO3::TangentVector w(vec);

  auto J = Lie::SO3::J_l(w);

  Eigen::Matrix3d J_true;
  J_true <<
      0.903487096381279,  -0.269965517979819,   0.248704942625503,
      0.340005136383935,   0.886918750941594,  -0.200968856375630,
     -0.138318605462201,   0.288289079054818,   0.928027378699932;      

  ASSERT_MATRICES_APPROX_EQ(J, J_true);
  
}

TEST(LeftJacobianTest, ZeroTest)
{
  Eigen::Vector3d vec;
  vec << 0, 0, 0;
  Lie::SO3::TangentVector w(vec);

  auto J = Lie::SO3::J_l(w);

  Eigen::Matrix3d J_true;
  J_true <<
      1, 0, 0,
      0, 1, 0,
      0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(J, J_true);
  
}


TEST(LeftJacobianInverseTest, Test1)
{
  Eigen::Vector3d vec;
  vec << 0.6280, 0.2920, 0.4317;
  Lie::SO3::TangentVector w(vec);

  auto Jinv = Lie::SO3::J_l_inv(w);

  Eigen::Matrix3d Jinv_true;
  Jinv_true <<
      0.977108945736626,   0.231303694806176,  -0.123152876548540,
      -0.200396305193824,   0.951058393109863,   0.324623184789532,
      0.168847123451460,  -0.303376815210468,   0.959578495515264;

  ASSERT_MATRICES_APPROX_EQ(Jinv, Jinv_true);
  
}

TEST(LeftJacobianInverseTest, ZeroTest)
{
  Eigen::Vector3d vec;
  vec << 0, 0, 0;
  Lie::SO3::TangentVector w(vec);

  auto Jinv = Lie::SO3::J_l_inv(w);

  Eigen::Matrix3d Jinv_true;
  Jinv_true <<
      1, 0, 0,
      0, 1, 0,
      0, 0, 1;

  ASSERT_MATRICES_APPROX_EQ(Jinv, Jinv_true);
}
