#include "lie/se3.hpp"

int main()
{
  double PI = 3.14159265359;

  /////////////////////////////////////////////////////
  // Constructing Lie Group Elements
  /////////////////////////////////////////////////////
  
  // A variety of constructors can be used to create
  // fixed (non-uncertaion)  Lie Group elements
  //
  // In addition to Lie::SE3 (shown here),
  // Lie::SO3, Lie::SE2, and Lie::SO2 have also been
  // implemented

  // Via Translation Parameters/Euler Angles
  Lie::SE3 T_g1_mu (1, 0, 0, 0.0, 0.0, PI/8);

  // Via Rotation (R) and translation (t) blocks
  Eigen::Matrix3d R;
  R <<
      1,         0,          1,
      0, cos(PI/8), -sin(PI/8),
      0, sin(PI/8),  cos(PI/8);
  Eigen::Vector3d t;
  t << 1, 0, 0;
  Lie::SE3 T_12_mu (R, t);

  // Via Direct Matrix representations
  Eigen::Matrix4d H_ij;
  H_ij <<
      1, 0, 0, 1,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1;
  Lie::SE3 T_ij_fixed(H_ij);
  
  Eigen::Matrix4d H_ik;    
  H_ik <<
      1, 0, 0, 2,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1;
  Lie::SE3 T_ik_mu(H_ik);

  
  ////////////////////////////////////////////////////////
  // Modeling Uncertain Elements
  ////////////////////////////////////////////////////////
  
  // Uncertainty can be modeled either jointly or
  // independently through the use of the
  // Lie::make_uncertain_state function

  // For independent random variables,
  // Lie::make_uncertain_state takes a single mean
  // value and block covariance and returns a
  // reference object that represents the uncertain
  // version of the lie group element

  // It is suggested that auto be used for the return type
  //  Lie::UncertainGroupElementRef< Lie::SE3, Lie::UncertainState<Lie::SE3> >
  auto T_ik = Lie::make_uncertain_state(
      T_ik_mu, Eigen::MatrixXd::Identity(6,6));
  
  
  // For Sets of Lie Group elements that are jointly
  // distributed in the Lie Algebra, the
  // Lie::make_uncertain_state function takes
  // a tuple of mean variables and single joint covariance
  // and returns a tuple of uncertain reference objects
  auto joint_refs = Lie::make_uncertain_state(
      std::make_tuple( T_g1_mu, T_12_mu),
      Eigen::MatrixXd::Identity(12,12));
  auto T_g1 = std::get<0>(joint_refs);
  auto T_12 = std::get<1>(joint_refs);  


  /////////////////////////////////////////////////////////
  // Performing Operations
  /////////////////////////////////////////////////////////
  
  // Operations including: pose composition, pose inverse,
  // and relative pose can be applied to both certain or
  // uncertain elements (or combinations thereof) via
  // the Lie::compose, Lie::between, and Lie::SE3.inverse() or
  // Lie::UncertainGroupElementRef.inverse() functions

  auto T_g2 = Lie::compose(T_g1, T_12);
  auto T_21 = T_12.inverse();
  auto T_jk = Lie::between(T_ij_fixed, T_ik);
  
  // The library will automatically determine whether or not
  // the input is known, independent, or jointly correlated
  // with the other inputs
  
  // If the result is uncertain, a Lie::UncertainGroupElementRef
  // object of the correct type will be returned
  // (Though currently from then on it will be assumed independent)

  
  ////////////////////////////////////////////////////////////
  // Accessing Resulting Mean and Covariance
  ////////////////////////////////////////////////////////////

  // If the resulting object is uncertain, the underlying mean
  // group element can be accessed through the mu() accessor function
  // and the associated marginal covariance can be accessed through
  // the marginal_cov() function

  Lie::SE3 T_jk_mean = T_jk.mu();
  Eigen::MatrixXd T_jk_cov = T_jk.marginal_cov();

  // Each fixed Lie Group element is callable, and calling it returns
  // the underlying matrix form

  std::cout << "T_jk:\n" << T_jk_mean() << "\n";
  std::cout << "T_jk Cov:\n" << T_jk_cov << "\n";



  
  // Conversion from existing coordinate based representations can be performed
  // using the unscented transform (See coord_change.hpp)
}

