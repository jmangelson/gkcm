%% SSC Test Script 
% This script is a Test Script for the SSC Functions:
% It runs each of the inverse, head2tail, tail2tail functions with a couple
% different inputs that can then be compared against another implementation

in1 = [1;2;3;4;5;6]
in2 = [3;6;299;12;0.5;3]

%% SSC Inverse
disp('Inverse Test 1: X_ij=in1')
[X_ji, Jminus] = ssc_inverse(in1)

disp ('Inverse Test 2: X_ij=in2')
[X_ji, Jminus] = ssc_inverse(in2)

%% SSC Head2Tail
disp ('Head2Tail Test 1: X_ij=in1 X_jk=in2')
[X_ik, Jplus] = ssc_head2tail(in1, in2)

disp ('Head2Tail Test 2: X_ij=in2 X_jk=in1')
[X_ik, Jplus] = ssc_head2tail(in2, in1)

%% SSC Tail2Tail
disp ('Tail2Tail Test 1: X_ij=in1 X_ik=in2')
[X_jk, Jtail] = ssc_tail2tail(in1, in2)

disp ('Tail2Tail Test 2: X_ij=in2 X_ik=in1')
[X_jk, Jtail] = ssc_tail2tail(in2, in1)


