#ifndef __UT_HPP
#define __UT_HPP

#undef NDEBUG
#include <vector>

#include <Eigen/Dense>

/////////////////////////////////////////////////////////////////////
// Misc functions such as minimized angle and circular mean
// (Not Used for Lie, but included for UT implementation generality)
namespace MISC
{

double minimized_angle(double angle)
{
  while(angle < -M_PI)
    angle = angle + 2.0*M_PI;
  
  while(angle >= M_PI)
    angle = angle - 2.0*M_PI;

  return angle;
}

// If angle_map is NULL it assumes none are angles
Eigen::VectorXd minimized_angle_vec(const Eigen::VectorXd& angles, std::vector<bool> angle_map={})
{
  int n = angles.rows();
  
  if(angle_map.size() == 0)
    return angles;

  Eigen::VectorXd minAngles(n);

  for(int i=0;i<n;i++){
    if(angle_map[i])
      minAngles(i) = minimized_angle(angles(i));
    else
      minAngles(i) = angles(i);
  } 

  return minAngles;
}

double circular_mean (const Eigen::VectorXd& samples)
{
  double n = (double)samples.rows();
  double sin_sum = (samples.array().sin()*(1.0/n)).sum();
  double cos_sum = (samples.array().cos()*(1.0/n)).sum();
  return atan2(sin_sum, cos_sum);
}

double circular_mean (const Eigen::VectorXd& samples, const Eigen::VectorXd& weights){
  double sin_sum = (samples.array().sin() * weights.array()).sum();
  double cos_sum = (samples.array().cos() * weights.array()).sum();
  return atan2(sin_sum, cos_sum);
}

}; // end namespace MISC

/////////////////////////////////////////////////////////
// Unscented Transform Implementation
namespace UT
{

///////////////////////////////////
// Input For Unscented Transform
class UnscentedTransformInput
{
 private:
  double alpha_;
  double kappa_;
  double beta_;
  
  double n_;
  double lambda_;
  
  Eigen::VectorXd mean_weight_;
  Eigen::VectorXd cov_weight_;
  Eigen::MatrixXd sigma_points_;


 public:
  UnscentedTransformInput(const Eigen::VectorXd& mu,
                          const Eigen::MatrixXd& Sigma,
                          double alpha=1.0,
                          double kappa=0.0,
                          double beta=2.0):
      alpha_(alpha),
      kappa_(kappa),
      beta_(beta),
      n_(mu.rows()),
      lambda_(alpha_*alpha_*(n_ + kappa_) - n_),
      mean_weight_(static_cast<int>(2*n_ + 1)),
      cov_weight_(static_cast<int>(2*n_ + 1)),
      sigma_points_(static_cast<int>(n_), static_cast<int>(2*n_+1))
  {
    Eigen::LLT<Eigen::MatrixXd> llt( Sigma*(n_ + lambda_) );
    Eigen::MatrixXd U = llt.matrixL();
    
    //Setup mean sigma point
    sigma_points_.col(0) = mu;
    mean_weight_(0) = lambda_/(n_ + lambda_);
    cov_weight_(0) = (lambda_/(n_ + lambda_)) + (1 - alpha_*alpha_ + beta_);

    //Setup other sigma points
    for(int i=1;i<=n_;i++){
      sigma_points_.col(i) = mu + U.col(i-1);
      mean_weight_(i) = 1.0/(2.0*(n_ + lambda_));
      cov_weight_(i) = 1.0/(2.0*(n_ + lambda_));
    }
    for(int i=n_+1;i<(2*n_+1);i++){
      sigma_points_.col(i) = mu - U.col(i-n_-1);
      mean_weight_(i) = 1.0/(2.0*(n_ + lambda_));
      cov_weight_(i) = 1.0/(2.0*(n_ + lambda_));
    }
  }

  const Eigen::MatrixXd& sigma_points() { return sigma_points_; }
  const Eigen::VectorXd& mean_weights() { return mean_weight_; }
  const Eigen::VectorXd& cov_weights() { return cov_weight_; }
};

///////////////////////////////////
// Output for Unscented Transform
class UnscentedTransformOutput
{
 public:
  Eigen::VectorXd mu;
  Eigen::MatrixXd Sigma;

  UnscentedTransformOutput(int n):
      mu(n),
      Sigma(n,n)
  {}
    
};

////////////////////////////////////
// Unscented Transform Evaluation
UnscentedTransformOutput
evaluate_unscented_transform(
    std::function<Eigen::VectorXd (const Eigen::VectorXd&)> transformation,
    UnscentedTransformInput& data,
    std::vector<bool> output_angle_map={})
{
  int n = data.sigma_points().cols();
  
  Eigen::VectorXd y0 = transformation(data.sigma_points().col(0));
  int m = y0.rows();

  Eigen::MatrixXd y(m,n);
  y.col(0) = y0;

  for(int i=1; i<n; i++)
    y.col(i) = transformation(data.sigma_points().col(i));

  UnscentedTransformOutput output(m);
  
  Eigen::VectorXd& mu_prime = output.mu;
  mu_prime = (y * data.mean_weights());
  if (output_angle_map.size() > 0){
    for(int i=0;i<m;i++){
      if(output_angle_map[i]){
        mu_prime(i) = MISC::circular_mean(y.row(i).transpose(), data.mean_weights());
        mu_prime(i) = MISC::minimized_angle(mu_prime(i));
      }
    }
  }

  Eigen::MatrixXd& sigma_prime = output.Sigma;
  sigma_prime = Eigen::MatrixXd::Zero(m,m);
  for(int i=0;i<n;i++){
    if(output_angle_map.size() == 0){
      sigma_prime += data.cov_weights()(i) * ( (y.col(i) - mu_prime) * 
                                               (y.col(i) - mu_prime).transpose() );
    }
    else{
      sigma_prime += data.cov_weights()(i) *
          ( MISC::minimized_angle_vec (y.col(i) - mu_prime, output_angle_map) *
            MISC::minimized_angle_vec (y.col(i) - mu_prime, output_angle_map).transpose() );
    }
  }

  return output;
}

}; // namespace UT

#endif //__UT_HPP
