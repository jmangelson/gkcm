# README #

### cuBlas-CPP ###

cuBlas-CPP is a cublas wrapper library that uses Eigen like templates to
represent matrices on CUDA enabled devices.

### How to build the code? ###

Required third party libs:

* [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)
* [CUDA >= 5.0 and CUDA CC >= 3.5]()

This software is constructed according to the [Pods software policies and templates](http://sourceforge.net/projects/pods).

To install:

**Local:** `make`. The project will search 4 levels up the tree for
a `build/` directory. If one is not found it will be installed to
`build/` in the local directory.

**System-wide:** `make BUILD_PREFIX=<build_location>` as root.


You can freely use this software as long as you cite our work.

### Questions or Contact ###

If you use this code in your research, we would love hear about it. 
In you have any questions, you can contact us via the following email:

Joshua Mangelson
mangelso@umich.edu
