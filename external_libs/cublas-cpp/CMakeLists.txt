cmake_minimum_required(VERSION 2.6.0)
set(POD_NAME cublas-cpp)

# pull in the pods macros. See cmake/pods.cmake for documentation
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/modules/")
include(cmake/pods.cmake)
include(cmake/util.cmake)

# CXX flags
add_flag(CMAKE_CXX_FLAGS "-std=c++14")

#Download, Install, and Setup GTEST if not already done
include(cmake/installgtest.cmake)
#Now simply link your own targets against gtest, gmock,
#gtest_main, gmock_main etc. as appropriate


# tell cmake to build these subdirectories (order matters)
add_subdirectory(src)
