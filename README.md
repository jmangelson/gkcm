# README #

This repository implements the method proposed in the following paper:

J. G. Mangelson, D. Dominic, R. M. Eustice, and R. Vasudevan, “Pairwise Consistent Measurement
Set Maximization for Robust Multi-robot Map Merging,” in Proceedings of the IEEE International Conference
on Robotics and Automation, Brisbane, Australia, May 2018.

### Pairwise Consistency Maximization (PCM) ###

Pairwise Consistency Maximization is a method for selecting a set of measurements to trust when performing
Multi-Agent Simultaneous Localization and Mapping (Multi-Agent SLAM). It does this by selecting the largest
set of measurements that are pairwise consistent with one another. By converting the problem into an instance
of the maximum clique problem, it can take advantage of existing maximum clique algorithms from graph theory.

While any maximum clique algorithm guaranteed to return a clique can be used, in this implementation
we use the algorithms proposed in: 

B. Pattabiraman, M. M. A. Patwary, A. H. Gebremedhin, W. keng Liao, and A. Choudhary, “Fast algorithms 
for the maximum clique problem on massive graphs with applications to overlapping community detection,” 
Internet Mathematics, vol. 11, no. 4-5, pp. 421–448, 2015.

### How to build the code? ###

This software is constructed according to the [Pods software policies and templates](http://sourceforge.net/projects/pods).

Required third party libs:

* [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)

To install:

**Local:** `make`. The project will search 4 levels up the tree for
a `build/` directory. If one is not found it will be installed to
`build/` in the local directory.

**System-wide:** `make BUILD_PREFIX=<build_location>` as root.

### Unit Testing
This project uses [GTest](https://github.com/google/googletest) for unit testing and will automatically download it locally when
you build.

Individual test binaries can be found in `test/` in the build directory.
`make test` will run all tests.

### License and Attribution

You can freely use this software as long as you cite both our work and that
of Pattabiraman et al. and follow the license and copywrite notices under 
LICENSE and FMC_COPYRIGHT. 

### Questions or Contact ###

If you use this code in your research, we would love hear about it. 
In you have any questions, you can contact us via the following email:

Joshua Mangelson
mangelso@umich.edu
